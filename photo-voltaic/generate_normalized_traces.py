#!/usr/bin/python3

# Generate the normalized traces used for the simulations

nb_traces = 35
# used to normalize the production, its the maximum consumption of a node
max_conso = 2.754
# in average how much of the maximum consumption is guarantee
production_factor = 0.5

def mean(l): 
    return sum(l) / len(l) 

def get_mean_production(tracefile):
  values = []
  f=open(tracefile,"r")
  for line in f:
    words = line.split(";")
    values.append(float(words[1]))
  f.close()
  return mean(values)
  
def get_max_production(tracefile):
  value = 0;
  f=open(tracefile,"r")
  for line in f:
    words = line.split(";")
    if float(words[1]) > value:
      value = float(words[1])
  return value

def write_production_normalized(tracefile, newtracefile, n):
  fin=open(tracefile,"r")
  fout=open(newtracefile,"w")
  for line in fin:
    words = line.split(";")
    fout.write(words[0] + ";" + str(n*float(words[1])) + "\n")
  fin.close()
  fout.close()

def generate_normalized_traces():
	mean_productions = [];
	max_productions = [];
	for i in range(1, nb_traces + 1):
		current_file = "traces/trace-" + str(i) + ".csv"
		mean_productions.append(get_mean_production(current_file))
		max_productions.append(get_max_production(current_file))
	normalization_value = max_conso * production_factor / mean(mean_productions)
	
	for i in range(1, nb_traces + 1):
		current_file = "traces/trace-" + str(i) + ".csv"
		output_file =  "normalized_traces/normalized_trace-" + str(i) + ".csv"
		write_production_normalized(current_file, output_file, normalization_value)

generate_normalized_traces()




