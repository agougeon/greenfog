#!/usr/bin/python3

# Generate the xml file used as platform for the simulation

import random
import numpy as np

outFile="italyISP.xml"
routes=list()
users_by_feeder = list()

BW_100GE=100 
BW_50GE=50
BW_20GE=20
BW_10GE=10

# Latency values from wondernetwork.com

# rotterdam - alblasserdam 2019-5-17-8h20 dist = 14km (not found below)
def LAT_CCI():
  return np.random.normal(0.721, 0.05)
def LAT_CCin():
  return np.random.normal(0.721, 0.05)
def LAT_BBBB():
  return np.random.normal(0.721, 0.05)
def LAT_MBB():
  return np.random.normal(0.721, 0.05)
def LAT_UF():
  return np.random.normal(0.721, 0.05)

# rotterdam - alblasserdam 2019-5-17-8h20 dist = 14km
def LAT_FM():
  return np.random.normal(0.721, 0.05)

# the hague - alblasserdam 2019-5-17-8h21 dist = 35km
def LAT_BBCin():
  return np.random.normal(1.478, 0.161)

# munich - frankfurt 2019-5-17-8h21 dist = 300km (+1 mean) (+1 std) -> 50-500 is very large so I increased variance
def LAT_BBCout():
  return np.random.normal(8.122, 1.552)

# paris  marseille 2019-5-17-7h21 dist = 610km
def LAT_CCout():
  return np.random.normal(18.607, 0.07)


def out(data,mode="a"):
    f=open(outFile,mode)
    f.write(data+'\n')
    f.close()

# wattage = idle + wifi idle + eth idle
# watts off = eth idle + wifi idle

def genHost(name):
    out('<host id="'+name+'" speed="100.0Mf, 0.0Mf, 0.0Mf" pstate="0" core="4" >\
<prop id="wattage_per_state" value="2.2694:2.2694:2.8891, 2.8891:2.8891:2.8891, 2.8891:2.8891:2.8891" />\
 <prop id="wattage_off" value="0.7814" />\
 <prop id="boot_time" value="150" />\
 <prop id="shut_time" value="10" />\
</host>')
def genLink(src,dst,bwGbps,lat,eMin=0,eMax=0,sharing="SPLITDUPLEX"):
    idle = 1.12
    byte = 3.4 * 1e-9
    pkt = 197.2 * 1e-9
    MTU = 1500
    BW = bwGbps * 1e9
    eMax = idle + (2 * ((byte + (pkt / MTU)) / 8)) * BW
    global routes
    out('<link id="'+src+"-"+dst+'" bandwidth="'+str(bwGbps * 1e3)+'Mbps" latency="'+str(lat)+'ms" \
sharing_policy="'+sharing.upper()+'">\
<prop id="wattage_range" value="'+str(idle)+':'+str(eMax)+'" />\
<prop id="wattage_off" value="0" />\
</link>')
    routes.append((src,dst))
def genRoutes():
    global routes
    for route in routes:
        out('<route src="'+route[0]+'" dst="'+route[1]+'" symmetrical="NO"><link_ctn id="'+route[0]+"-"+route[1]+"_UP" +'" /></route>')
        out('<route src="'+route[1]+'" dst="'+route[0]+'" symmetrical="NO"><link_ctn id="'+route[0]+"-"+route[1]+"_DOWN" +'" /></route>')

def linkBBC(BBa,Ca,Cb):
    """ Link BBa to Ca and BBb to Cb and link BBa and BBa+1 together. """
    BBb=BBa+1;BBa=str(BBa);BBb=str(BBb);Ca=str(Ca);Cb=str(Cb)
    genLink("BB"+BBa,"C"+Ca,BW_20GE,LAT_BBCin())
    genLink("BB"+BBa,"BB"+BBb,BW_20GE,LAT_BBBB())  # Link Backbone together
    genLink("BB"+BBb,"C"+Cb,BW_20GE,LAT_BBCout())

    
# Start a new file
out('<?xml version=\'1.0\'?>\n\
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">\n\
<platform version="4.1">\n<AS id="AS0" routing="Full">',"w")

# Create Internet node
genHost("I")
# Create Core nodes
for i in range(0,8):
    genHost("C"+str(i))
# Create Backbone nodes
for i in range(0,52):
    genHost("BB"+str(i))
# Create Metro nodes
for i in range(0,52):
    genHost("M"+str(i))
# Create Feader nodes
for i in range(0,260):
    genHost("F"+str(i))
    r = random.randint(5,10)
    users_by_feeder.append(r)
for i in range(0,260):
  r = users_by_feeder[i]
  for j in range(0,r):
      genHost("U"+str(i)+"_"+str(j))
    
# Link Internet to Core nodes
genLink("I","C0",BW_100GE,0)
genLink("I","C1",BW_100GE,0)
genLink("I","C2",BW_100GE,0)
genLink("I","C3",BW_100GE,0)

# Link Cores nodes to each others
genLink("C0","C1",BW_50GE,LAT_CCin());genLink("C1","C2",BW_50GE,LAT_CCout());genLink("C2","C3",BW_50GE,LAT_CCin());genLink("C3","C4",BW_50GE,LAT_CCout())
genLink("C4","C5",BW_50GE,LAT_CCin());genLink("C5","C6",BW_50GE,LAT_CCout());genLink("C6","C7",BW_50GE,LAT_CCin());genLink("C7","C0",BW_50GE,LAT_CCout())
genLink("C0","C3",BW_50GE,LAT_CCout());genLink("C2","C5",BW_50GE,LAT_CCout());genLink("C4","C7",BW_50GE,LAT_CCout());genLink("C6","C1",BW_50GE,LAT_CCout()) # Redundancy

# Backbone to Core
for i in range(0,52,2):
    if i<=13:
        linkBBC(i,1,7) # Link BBi to BBi+1 and link BBi and BBi+1 to C1 and C7
    elif i>13 and i<=25:
        linkBBC(i,2,4)
    elif i>25 and i<=37:
        linkBBC(i,5,3)
    else:
        linkBBC(i,6,0)

# Metro to Backbone
for i in range(0,52,2):
    # Metro i
    genLink("M"+str(i),"BB"+str(i),BW_20GE,LAT_MBB())
    genLink("M"+str(i),"BB"+str(i+1),BW_20GE,LAT_MBB())
    # Metro i+1
    genLink("M"+str(i+1),"BB"+str(i+1),BW_20GE,LAT_MBB())
    genLink("M"+str(i+1),"BB"+str(i),BW_20GE,LAT_MBB())

# Feeders to Metro
BBid=0
for i in range(0,260):
    if (i%10==0) and (i!=0):
        BBid=BBid+2 # Go to the next Metro every 10 feeders (there is 10 feeders per pair of Metro)
    genLink("F"+str(i),"M"+str(BBid),BW_10GE,LAT_FM())
    genLink("F"+str(i),"M"+str(BBid+1),BW_10GE,LAT_FM())

# Users to Feeders
for i in range(0,260):
  for j in range(0,users_by_feeder[i]):
    genLink("U"+str(i)+"_"+str(j),"F"+str(i),BW_10GE,LAT_UF())

# End file
genRoutes()
out('</AS>\n</platform>')


# Print info
print("----- Platform Informations -----")
print("8"+'{:>14}'.format("Core Nodes"))
print("52"+'{:>18}'.format("Backbones Nodes"))
print("52" +'{:>14}'.format("Metro Nodes"))
print("260"+'{:>15}'.format("Feeders Nodes"))
print(str(sum(users_by_feeder))+'{:>12}'.format("Users Nodes"))
print("Totally "+str(8+52+52+260+sum(users_by_feeder))+" nodes and "+str(len(routes))+" links")
print("---------------------------------")
