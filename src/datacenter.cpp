#include "simgrid/s4u.hpp"
#include "simgrid/plugins/energy.h"

#include "datacenter.hpp"
#include "green_host.hpp"

#include <fstream>



XBT_LOG_NEW_DEFAULT_CATEGORY(datacenter, "messages specific to the datacenter class");


DataCenter::DataCenter(std::string city, std::vector<simgrid::s4u::Host*> hosts)
{
    city_ = city;
    hosts_ = hosts;
    hosts_.front()->turn_on();
    hosts_.front()->extension<GreenHost>()->set_state(GreenHost::state::ON);
    for (auto const& host: hosts_)
    {
        host->extension<GreenHost>()->set_dc(this);
        power_consumption_ += sg_host_get_current_consumption(host);
    }
}

std::string DataCenter::get_city()
{
    return city_;
}

std::vector<simgrid::s4u::Host*> DataCenter::get_hosts()
{
    return hosts_;
}

void DataCenter::set_power_consumption(double power)
{
    power_consumption_ = power;
}

double DataCenter::get_power_consumption()
{
    return power_consumption_;
}

void DataCenter::set_green_power_production(double power)
{
    green_power_production_ = power;
}

double DataCenter::get_green_power_available()
{
    return std::max(green_power_production_ - power_consumption_, 0.0);
}

double DataCenter::get_green_energy_consumed()
{
    return green_energy_consumed_;
}

double DataCenter::get_brown_energy_consumed()
{
    return brown_energy_consumed_;
}

void DataCenter::update_consumed_energy()
{
    int now = simgrid::s4u::Engine::get_instance()->get_clock();
    power_consumption_ = 0;
    if (now > last_update_)
    {
        for (auto const& host: hosts_)
        {
            power_consumption_ += sg_host_get_current_consumption(host);
        }
        double energy_consumed = power_consumption_ * (now - last_update_);
        double green_energy_produced = green_power_production_ * (now - last_update_);
        brown_energy_consumed_ += std::max(energy_consumed - green_energy_produced, 0.0);
        green_energy_consumed_ += std::min(energy_consumed, green_energy_produced);
        last_update_ = now;
    }
}

void pv_production_actor(std::string file, DataCenter* dc)
{
    std::ifstream infile(file);
    std::string time;
    std::string production;
    while(std::getline(infile, time, ';'))
    {
        dc->update_consumed_energy();
        std::getline(infile, production, '\n');
        simgrid::s4u::this_actor::sleep_until(atoi(time.c_str()));
        dc->set_green_power_production(atof(production.c_str()));
    }
}
