#ifndef INIT_DC
#define INIT_DC

#include "simgrid/s4u.hpp"
#include "datacenter.hpp"

/* Master actor
 *
 * Schedules jobs and consolidate hosts depending on the parameters
 *
 * @param hosts: the hosts to manage
 * @param scheduling_policy: the scheduling policy chosen
 * @param conso_policy: the consolidation policy chosen
 * @param jobs_file: a file containing the jobs to schedule
 *
 * ATM the job file look like that:
 * ARRIVAL_job1 CORES_job1 DURATION_job1
 * ARRIVAL_job2 CORES_job2 DURATION_job2
 * etc...
 */
void master_dc(std::vector<DataCenter*> dcs, int scheduling_policy, int conso_policy, std::string jobs_file);

/* Simulation of the system depending on the arguments given
 *
 * Initialize plugins,
 * check arguments given,
 * instantiate physical machine for the simulation,
 * launch the master actor,
 * run the simulation.
 *
 * Return a pair with the simulation engine as first
 * and th vector of physical hosts as second.
 *
 * This function must be used in the main executable and its arguments
 * are the arguments of the main function.
 */
std::pair <simgrid::s4u::Engine*, std::vector<simgrid::s4u::Host*>> simulation_dc(int argc, char* argv[]);

#endif
