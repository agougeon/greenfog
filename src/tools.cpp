#include "simgrid/s4u.hpp"
#include "simgrid/s4u/VirtualMachine.hpp"
#include "simgrid/plugins/live_migration.h"

#include "green_host.hpp"
#include "datacenter.hpp"
#include "tools.hpp"

#include <boost/algorithm/string.hpp>
#include <math.h>
#include <fstream>
#include <sys/stat.h>

XBT_LOG_NEW_DEFAULT_CATEGORY(tools,"messages specific to the tools part");

/* Sort an array [][3] by the first value of the second dimension
 *
 * @param array: the 2D array to sort
 * @param fst_dim: size of the first dimension of the array
 */
void sort(int array[][3], int fst_dim)
{
    int out,in,temp,temp2;
    for(out=0;out<fst_dim-1;out++)
    {
        for(in=out+1;in<fst_dim;in++)
        {
            if(array[in][0]<array[out][0])
            {
                temp=array[out][0];
                temp2=array[out][1];
                array[out][0]=array[in][0];
                array[out][1]=array[in][1];
                array[in][0]=temp;
                array[in][1]=temp2;
            }
        }
    }
}

/* Print information about the hosts and vms in the simulation
 * Example:
 *
 * HOSTS
 * Name:A Load:B State:C
 * Name:D ...
 *
 * VMS
 * Name:E Load:F State:G
 * + actor: I
 * + actor: J
 * ...
 * Name: H ...
 *
 * @param hosts: the physical hosts of the simulation
 */
void print_status(std::vector<simgrid::s4u::Host*> hosts)
{
    XBT_INFO("HOSTS");
    for (auto const& host : hosts)
    {
        XBT_INFO("Name:%s Load:%f State:%s", host->get_name().c_str(), host->get_load(), host->extension<GreenHost>()->state_string().c_str());
        for( auto const& actor: host->get_all_actors())
            XBT_INFO("+ actor: %s", actor->get_name().c_str());
    }
    XBT_INFO("VMS");
    for (auto const& host : simgrid::s4u::Engine::get_instance()->get_all_hosts())
    {
        if (dynamic_cast<simgrid::s4u::VirtualMachine*> (host))
        {
            XBT_INFO("Name:%s Host:%s Load:%f State: %d", host->get_name().c_str(), dynamic_cast<simgrid::s4u::VirtualMachine*> (host)->get_pm()->get_name().c_str(),host->get_load(), ((simgrid::s4u::VirtualMachine*)host)->get_state());
            for( auto const& actor: host->get_all_actors())
                XBT_INFO("+ actor: %s", actor->get_name().c_str());
        }
    }
}

/* Build a vector of datacenters with the given hosts, sorted by city,
 * Each city correspond to 1 datacenter
 *
 * @param hosts: the hosts to split into datacenters
 * @return: the vector
 */
std::vector<DataCenter*> split_hosts_by_datacenter(std::vector<simgrid::s4u::Host*> hosts)
{
    std::map<std::string, std::vector<simgrid::s4u::Host*>> map;
    std::vector<DataCenter*> dcs;
    std::vector<std::string> str_list;
    std::map<std::string, std::vector<simgrid::s4u::Host*>>::iterator itr;

    for (auto const& host: hosts)
    {
        boost::split(str_list, host->get_name(), [](char c){return c == '.';});
        itr = map.find(str_list[1]);
        if (itr != map.end())
            itr->second.push_back(host);
        else
            map[str_list[1]] = std::vector<simgrid::s4u::Host*> {host};
    }

    for (auto const& m: map)
    {
        dcs.push_back(new DataCenter(m.first, m.second));
    }

    return dcs;
}

/* Print the energy consumed by the datacenter
 * Example:
 *
 * Energy consumed by the datacenters
 * DC1 (x machines): G:green_energy B:brown_energy
 * DC2 ...
 * total: t G:y B:z
 *
 * @param dcs: the datacenters
 */
void print_consumed_energy_dc(std::vector<DataCenter*> dcs)
{
    double total_green = 0;
    double total_brown = 0;
    XBT_INFO("Energy consumed by the datacenters:");
    for (auto dc: dcs)
    {
        total_green += dc->get_green_energy_consumed();
        total_brown += dc->get_brown_energy_consumed();
        XBT_INFO("%s (%d machines): G: %f B: %f", dc->get_city().c_str(), dc->get_hosts().size(),dc->get_green_energy_consumed(), dc->get_brown_energy_consumed());
    }
    XBT_INFO("total: %f G: %f B: %f", total_green + total_brown, total_green, total_brown);
}

/* Print the energy consumed by the hosts
 * Example:
 *
 * Energy consumed by the Hosts:
 * HOST1: G:green_energy B:brown_energy
 * HOST2 ...
 * total: t G:y B:z
 *
 * @param hosts: the hosts
 */
void print_consumed_energy_hosts(std::vector<simgrid::s4u::Host*> hosts)
{
    double total_green = 0;
    double total_brown = 0;

    XBT_INFO("Energy consumed by the Hosts:");
    for (auto host: hosts)
    {
        total_green += host->extension<GreenHost>()->get_green_energy_consumed();
        total_brown += host->extension<GreenHost>()->get_brown_energy_consumed();
        XBT_INFO("%s: G: %f B: %f", host->get_name().c_str(), host->extension<GreenHost>()->get_green_energy_consumed(), host->extension<GreenHost>()->get_brown_energy_consumed());
    }
    XBT_INFO("total: %f G: %f B: %f", total_green + total_brown, total_green, total_brown);
}

/* Write the energy consumed by the hosts into a file called "consumed_energy.txt"
 * Example:
 *
 * name,green,brown
 * HOST1,a,b
 * ...
 * Platform,total_green,total_brown
 *
 *
 * @param hosts: the hosts
 * @param folder_name: path to where the file must be placed
 */
void write_consumed_energy_hosts(std::vector<simgrid::s4u::Host*> hosts, std::string folder_name)
{
    double total_green = 0;
    double total_brown = 0;
    double total_links = 0;
    std::ofstream file;
    file.open(folder_name + "/consumed_energy.txt");
    file << "name,green,brown\n";

    for (auto host: hosts)
    {
        total_green += host->extension<GreenHost>()->get_green_energy_consumed();
        total_brown += host->extension<GreenHost>()->get_brown_energy_consumed();
//        file << host->get_name() << ","
//             << std::to_string(host->extension<GreenHost>()->get_green_energy_consumed()) << ","
//             << std::to_string(host->extension<GreenHost>()->get_brown_energy_consumed()) << "\n";
    }

    file << "Hosts," <<  std::to_string(total_green) << "," << std::to_string(total_brown) << std::endl;

    total_links = get_links_energy_consumed() - simgrid::s4u::Engine::get_instance()->get_all_hosts().front()->extension<GreenHost>()->links_energy_consumed_before_log_;

    file << "Links," <<  std::to_string(0) << "," << std::to_string(total_links) << std::endl;
    file << "Platform," <<  std::to_string(total_green) << "," << std::to_string(total_brown + total_links);

    file.close();
}

/* Print the latency summary of the hosts
 * Example:
 *
 * Initial latency summary: total= t1,  mean= m1, variance= v1, std= s1
 * Final latency summary: total= t2,  mean= m2, variance= v2, std= s2
 *
 * @param hosts: the hosts
 */
void print_latency_summary(std::vector<simgrid::s4u::Host*> hosts)
{
    double var_i = 0;
    double var_f = 0;
    double sum_i = 0;
    double sum_f = 0;
    double mean_i = 0;
    double mean_f = 0;
    double std_i = 0;
    double std_f = 0;
    int i = 0;
    for (auto const& host: hosts)
    {
        for (auto const& l: host->extension<GreenHost>()->get_latencies_initial())
        {
            sum_i += l;
            i++;
        }
        for (auto const& l: host->extension<GreenHost>()->get_latencies_final())
        {
            sum_f += l;
        }
    }
    mean_i = sum_i/i;
    mean_f = sum_f/i;
    for (auto const& host: hosts)
    {
        for (auto const& l: host->extension<GreenHost>()->get_latencies_initial())
        {
            var_i += (l - mean_i) * (l - mean_i);
        }
        for (auto const& l: host->extension<GreenHost>()->get_latencies_final())
        {
            var_f += (l - mean_f) * (l - mean_f);
        }
    }
    var_i /= i;
    var_f /= i;
    std_i = sqrt(var_i);
    std_f = sqrt(var_f);
    XBT_INFO("Initial latency summary: total= %f,  mean= %f, variance= %f, std= %f", sum_i, mean_i, var_i, std_i);
    XBT_INFO("Final latency summary: total= %f,  mean= %f, variance= %f, std= %f", sum_f, mean_f, var_f, std_f);
}

/* Write the latency summary of the hosts into a file called "latency_summary.txt"
 * Example:
 *
 * data_type,sum,mean,var,std
 * latency_initial,s1,m1,v1,s1
 * latency_final,...
 * nb_messages,x,0,0,0
 *
 * @param hosts: the hosts
 * @param folder_name: path to where the file must be placed
 */
void write_latency_summary(std::vector<simgrid::s4u::Host*> hosts, std::string folder_name)
{
    int jobs_awakening_host = 0;
    int jobs_not_awakening_host = 0;
    double l_ms;
    int jobs_per_lat[20] = {0};
    int jobs_per_sector[5] = {0};
//    double sum_final_lat_jobs_not_in_place = 0;
    long messages;
    for (auto const& host: hosts)
    {
        for (auto const& l: host->extension<GreenHost>()->get_latencies_initial())
        {
            if (l > 150)
                jobs_awakening_host++;
            else
                jobs_not_awakening_host++;

        }
        for (auto const& l: host->extension<GreenHost>()->get_latencies_final()) {
//            if (l == 0) {
//                jobs_in_place++;
//                sum_final_lat_jobs_not_in_place += l;
//            }
//            else {
//                jobs_not_in_place++;
//                sum_final_lat_jobs_not_in_place += l;
//            }
            l_ms = l * 1000;
            jobs_per_lat[(int) round((l_ms))]++;
// Values here are from the means values and standard deviations in the platform italyISP.xml
            if (l_ms == 0)
                jobs_per_sector[0]++;
            else if (l_ms > 1.142 && l_ms < 1.742)
                jobs_per_sector[1]++;
            else if (l_ms > 2.284 && l_ms < 3.484)
                jobs_per_sector[2]++;
            else if (l_ms > 5.416 && l_ms < 9.148)
                jobs_per_sector[3]++;
            else if (l_ms > 9.148  && l_ms < 20.836)
                jobs_per_sector[4]++;
            else
                XBT_INFO("weird latency found: %f", l_ms);
        }
        messages += host->extension<GreenHost>()->get_message_sent();
    }
//    double mean_final_lat_jobs_not_in_place = sum_final_lat_jobs_not_in_place / (jobs_not_in_place + jobs_in_place) * 1000;
//    double var_final_lat_not_in_place = 0;
//    for (auto const& host: hosts)
//    {
//        for (auto const& l: host->extension<GreenHost>()->get_latencies_final())
//        {
//            var_final_lat_not_in_place += (l * 1000 - mean_final_lat_jobs_not_in_place) * (l * 1000 - mean_final_lat_jobs_not_in_place);
//        }
//    }
//    var_final_lat_not_in_place /= (jobs_not_in_place + jobs_in_place);

    std::ofstream file;
    file.open(folder_name + "/latency_summary.txt");
    // header
    for (int i = 0; i < sizeof(jobs_per_lat)/sizeof(jobs_per_lat[0]); i++)
        file << "jobs_" << i << "ms,";
    for (int i = 0; i < sizeof(jobs_per_sector)/sizeof(jobs_per_sector[0]); i++)
        file << "jobs_sector_" << i << ",";
    file << "jobs_awakening_host,jobs_not_awakening_host,messages\n";

    //values
    for (int i = 0; i < sizeof(jobs_per_lat)/sizeof(jobs_per_lat[0]); i++)
        file << jobs_per_lat[i] << ",";
    for (int i = 0; i < sizeof(jobs_per_sector)/sizeof(jobs_per_sector[0]); i++)
        file << jobs_per_sector[i] << ",";
    file << std::to_string(jobs_awakening_host) << "," << std::to_string(jobs_not_awakening_host) << "," << std::to_string(messages);
    file.close();


//    double var_i = 0;
//    double var_f = 0;
//    double sum_i = 0;
//    double sum_f = 0;
//    double mean_i = 0;
//    double mean_f = 0;
//    double std_i = 0;
//    double std_f = 0;
//    int messages_sent = 0;
//    int i = 0;
//    int nb_above = 0;
//    int nb_below = 0;
//    int nb_in_place = 0;
//    for (auto const& host: hosts)
//    {
//        for (auto const& l: host->extension<GreenHost>()->get_latencies_initial())
//        {
//            sum_i += l;
//            i++;
//            if (l > 150)
//                nb_above++;
//            else
//                nb_below++;
//            if (l == 0 || l == 150)
//                nb_in_place++;
//        }
//        for (auto const& l: host->extension<GreenHost>()->get_latencies_final())
//        {
//            sum_f += l;
//        }
//        messages_sent += host->extension<GreenHost>()->get_message_sent();
//    }
//    mean_i = sum_i/i;
//    mean_f = sum_f/i;
//    for (auto const& host: hosts)
//    {
//        for (auto const& l: host->extension<GreenHost>()->get_latencies_initial())
//        {
//            var_i += (l - mean_i) * (l - mean_i);
//        }
//        for (auto const& l: host->extension<GreenHost>()->get_latencies_final())
//        {
//            var_f += (l - mean_f) * (l - mean_f);
//        }
//    }
//    var_i /= i;
//    var_f /= i;
//    std_i = sqrt(var_i);
//    std_f = sqrt(var_f);
//    std::ofstream file;
//    file.open(folder_name + "/latency_summary.txt");
//    file << "data_type,sum,mean,var,std\n";
//    file << "initial_latency," << std::to_string(sum_i) << "," << std::to_string(mean_i) << ","
//         << std::to_string(var_i) << "," << std::to_string(std_i) << "\n"
//         << "final_latency," << std::to_string(sum_f) << "," << std::to_string(mean_f) << ","
//         << std::to_string(var_f) << "," << std::to_string(std_f) << "\n"
//         << "jobs_awaking_node," << std::to_string(nb_above) << ",0,0,0\n"
//         << "jobs_not_awaking_node," << std::to_string(nb_below) << ",0,0,0\n"
//         << "jobs_in_place," << std::to_string(nb_in_place) << ",0,0,0\n"
//         << "nb_messages," << std::to_string(messages_sent) << ",0,0,0";
//    file.close();
}



/* Add to each host the list of its neighboors with corresponding latency
 *
 * @param hosts: the hosts
 */
void compute_routes(std::vector<simgrid::s4u::Host*> hosts)
{
    std::vector<simgrid::s4u::Link*> links;
    double latency;

    for (auto const& h1: hosts)
    {
        for (auto const& h2:hosts)
        {
            links.clear();
            latency = 0;
            if (h1 != h2)
                h1->route_to(h2, links, &latency);
            if ((links.size() == 1) && latency > 0)
                h1->extension<GreenHost>()->add_neighboor(h2, latency);
        }
    }
}

/* TODO
 *
 */
void create_logs(std::string log_folder, std::string platform_file, std::string config_file, std::string jobs_file)
{
    //  time_t rawtime;
    //  struct tm * timeinfo;
    //  char date[20];
    //  time (&rawtime);
    //  timeinfo = localtime(&rawtime);
    //  strftime (date,20,"%d-%m-%y_%H-%M-%S",timeinfo);
    //  std::string s(date);
    //  std::string folder_name = "../logs/"+ prefix + s + "/";
    mkdir(log_folder.c_str(), 0777);

    std::ifstream f(config_file);
    std::string line;
    getline(f, line);

    std::string path = platform_file;
    std::size_t botDirPos = path.find_last_of("/");
    std::string p = path.substr(botDirPos+1, path.length());
    path = jobs_file;
    botDirPos = path.find_last_of("/");
    std::string j = path.substr(botDirPos+1, path.length());

    std::ofstream resume_file(log_folder + "/resume.txt");
    resume_file << line + "\n";
    resume_file << "platform: " + p + "\n";
    resume_file << "jobs: " + j + "\n";
    resume_file.close();

    std::ofstream platform_power(log_folder + "/platform_power.txt");
    platform_power << "time,power_consumption,brown_consumption,green_production,green_exploitable\n";
}

/* Initialize the various plugins
 */
void plugins_init() {
    XBT_VERB("Init energy plugin");
    sg_host_energy_plugin_init();
    XBT_VERB("Init green vm plugin");
    sg_green_vm_init();
    XBT_VERB("Init green host plugin");
    sg_green_host_init();
    XBT_VERB("Init migration plugin");
    sg_vm_live_migration_plugin_init();
    XBT_VERB("Init energy link plugin");
    sg_link_energy_plugin_init();
}

/* Check the command line parameters
 * Exit if not OK
 */
void argument_checker(int argc, char* argv[]) {
    XBT_VERB("Checking arguments");
    xbt_assert(argc > 4, "Usage: %s platform_file config_file jobs_file log_folder\n", argv[0]);
    for (unsigned i = 1; i < argc - 2; i++) {
        std::ifstream f(argv[i]);
        xbt_assert(f.is_open(), "Could not open file %s.", argv[i]);
        f.close();
    }
}

/* Return a vector of arguments from the config file, if arguments are OK,
 * otherwise exit
 *
 * @param config_file: path to the config file
 */
std::vector<double> config_parameters(std::string config_file) {
    XBT_VERB("Parsing config file");
    std::ifstream f(config_file);
    int init_state, scheduling_policy, consolidation_policy, green_producers;
    double latency_range, ratio, threshold;
    while (f >> init_state >> scheduling_policy >> consolidation_policy >> green_producers >> latency_range >> ratio >> threshold){}
    xbt_assert(init_state >= 0 || init_state <= 1, "Wrong initial state. Expected [0,1] got %d", init_state);
    xbt_assert(scheduling_policy >= 0 || scheduling_policy <= 3, "Wrong scheduling policy. Expected [0,3] got %d", scheduling_policy);
    xbt_assert(consolidation_policy >= 0 || consolidation_policy <= 3, "Wrong consolidation policy. Expected [0,3] got %d", consolidation_policy);
    xbt_assert(green_producers >= 0, "Wrong number of green producers. Expected [0,inf[ got %d", green_producers);
    xbt_assert(scheduling_policy == 0 || latency_range >= 0, "Wrong latency range. Expected [0, inf[ got %f", latency_range);
    xbt_assert(consolidation_policy != 3 || (ratio >= 0 || ratio <= 1), "Wrong consolidation ratio. Expected [0,1] got %f", ratio);
    xbt_assert(consolidation_policy != 3 || (threshold > 0 || threshold <= 1), "Wrong consolidation threshold. Expected ]0,1] got %f", threshold);
    std::vector<double> v {(double) init_state,(double) scheduling_policy, (double) consolidation_policy, (double) green_producers, latency_range, ratio, threshold};
    if (init_state)
        XBT_INFO("init_state: hosts ON");
    else
        XBT_INFO("init_state: hosts OFF");
    switch (scheduling_policy) {
        case 0: XBT_INFO("scheduling policy: Lowest Latency"); break;
        case 1: XBT_INFO("scheduling policy: First Green"); break;
        case 2: XBT_INFO("scheduling policy: Most Green"); break;
    }
    switch (consolidation_policy) {
        case 0: XBT_INFO("consolidation policy: No Consolidation"); break;
        case 1: XBT_INFO("consolidation policy: Consolidate All"); break;
        case 2: XBT_INFO("consolidation policy: Consolidate Brown"); break;
        case 3: XBT_INFO("consolidation policy: Consolidate Ratio"); break;
    }
    XBT_INFO("green producers: %d", green_producers);
    XBT_INFO("latency range: %f", latency_range);
    if (consolidation_policy == 3) {
        XBT_INFO("ratio (policy Consolidate Ratio): %f", ratio);
        XBT_INFO("threshold (policy Consolidation Ratio): %f", threshold);
    }
    return v;
}

/* Initialize the hosts by shutting them down if needed
 *
 * @param init_state: initial state of the hosts (0 = OFF, 1 = ON)
 * @param hosts: vector of hosts to initialize
 */
void init_hosts(int init_state, int start_log) {
    XBT_VERB("Initializing physical machines");
    std::vector<simgrid::s4u::Host*> hosts = simgrid::s4u::Engine::get_instance()->get_all_hosts();
    for (auto const& host:hosts)
    {
        host->extension<GreenHost>()->set_last_update(start_log);
        // Setting which host is valid to run jobs
        // Here only users nodes "U" are valid
        if (host->get_name()[0] != 'U')
        {
            host->extension<GreenHost>()->set_valid_host(false);
        }
        // Turning off hosts if needed
        if (!init_state)
        {
            host->turn_off();
            host->extension<GreenHost>()->set_state(GreenHost::state::OFF);
        }
    }
    // The first host must always be ON (it hosts the master actor)
    hosts.front()->turn_on();
    hosts.front()->extension<GreenHost>()->set_state(GreenHost::state::ON);

    compute_routes(hosts);
}

/* Connection to the signals
 */
void signals_connection(std::string log_folder, std::vector<simgrid::s4u::Host*> hosts, int start_log, int end_log) {
    on_power_change.connect([log_folder, hosts, start_log, end_log](simgrid::s4u::Host& host)
    {
        host.extension<GreenHost>()->update_logs(hosts, log_folder, start_log, end_log);
        host.extension<GreenHost>()->update_consumed_energy();
    });
    on_green_power_change.connect([log_folder, hosts, start_log, end_log](simgrid::s4u::Host& host)
    {
        host.extension<GreenHost>()->update_logs(hosts, log_folder, start_log, end_log);
        host.extension<GreenHost>()->update_consumed_energy();
    });
    simgrid::s4u::Engine::on_simulation_end.connect([log_folder, hosts, start_log, end_log]()
    {
        hosts.front()->extension<GreenHost>()->update_logs(hosts, log_folder, start_log, end_log);
    });
}

/* Define which consolidation policy to adopt when a vm is removed from a host
 */
void apply_consolidation_policy(int consolidation_policy, double ratio, double threshold) {
    XBT_VERB("Selecting consolidation policy");
    static double r = ratio;
    static double t = threshold;
    switch (consolidation_policy)
    {
    case 0:
        break;
    case 1:
        on_vm_removed.connect([](simgrid::s4u::Host& host)
        {
            if (&host != simgrid::s4u::Engine::get_instance()->get_all_hosts().front())
                host.extension<GreenHost>()->consolidate();
        });
        break;
    case 2:
        on_vm_removed.connect([](simgrid::s4u::Host& host)
        {
            if (&host != simgrid::s4u::Engine::get_instance()->get_all_hosts().front())
                host.extension<GreenHost>()->consolidate_brown();
        });
        break;
    case 3:
        on_vm_removed.connect([&](simgrid::s4u::Host& host)
        {
            if (&host != simgrid::s4u::Engine::get_instance()->get_all_hosts().front())
                host.extension<GreenHost>()->consolidate_ratio(r, t);
        });
        break;
    default:
        XBT_INFO("Wrong consolidation policy provided (%d)", consolidation_policy);
    }
}

/* Count the number of jobs in a job file
 *
 * @return: number fo jobs
 */
int count_jobs(std::string jobs_file) {
    int nb_jobs = 0;
    std::string line;
    std::ifstream file(jobs_file);
    while (getline(file, line))
        nb_jobs++;
    return nb_jobs;
}

/* Compute a weighted placement vector
 * If a host as a weight of 3 it is placed 3 time in the vector
 *
 * @return: the vector
 */
std::vector<int> get_weight_vector() {
    std::ifstream weightfile("../configs/weights_placement.txt");
    std::vector<int> weight_vector;
    int weight;
    int n = 0;
    while (weightfile >> weight)
    {
        for (int i = 0; i < weight; i++)
        {
            weight_vector.push_back(n);
        }
        n++;
    }
    return weight_vector;
}

/* Return the total energy consumed by the links
 */
double get_links_energy_consumed(void) {
    double total_links;
    for (auto link: simgrid::s4u::Engine::get_instance()->get_all_links())
    {
        total_links += sg_link_get_consumed_energy(link);
    }
    return total_links;
}
