#ifndef CONSOLIDATION
#define CONSOLIDATION

#include "simgrid/s4u.hpp"

#include "datacenter.hpp"

void consolidate(std::vector<DataCenter*> dcs, int conso_policy);

/* Apply consolidation on the hosts, depending on the chosen policy
 *
 * @param hosts: the vector of hosts to consolidate
 * @param conso_policy: the consolidation policy
 *
 * (0 = no consolidation,
 *  1 = consolidation using first fit optimized to place vms,
 *  2 = consolidation using mbfd to place vms)
 *
 * Consolidation only consider underutilized hosts for the moment
 */
void consolidate(std::vector<simgrid::s4u::Host*> hosts, int conso_policy);

/* Consolidate hosts by shutting down empty hosts and use the first fit 
 * optimized policy to move vms from underutilized hosts.
 *
 * @param hosts: the vector of hosts to consolidate
 */
void close_underutilized_hosts_first_fit_optimized(std::vector<simgrid::s4u::Host*> hosts);

/* Consolidate hosts by shutting down empty hosts and use the mbfd 
 * policy to move vms from underutilized hosts.
 *
 * @param hosts: the vector of hosts to consolidate
 */
void close_underutilized_hosts_mbfd(std::vector<simgrid::s4u::Host*> hosts);


// ------------- DEPRECATED -----------

void closing_actor(std::vector<simgrid::s4u::Host*> hosts);

void consolidation_actor(std::vector<simgrid::s4u::Host*> hosts, int conso_policy, int period);

#endif
