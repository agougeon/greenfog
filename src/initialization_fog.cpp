#include "simgrid/s4u.hpp"
#include "simgrid/plugins/energy.h"
#include "simgrid/s4u/VirtualMachine.hpp"
#include "simgrid/plugins/live_migration.h"

#include "scheduling.hpp"
#include "consolidation.hpp"
#include "green_host.hpp"
#include "tools.hpp"

#include <fstream>

XBT_LOG_NEW_DEFAULT_CATEGORY(initialization_fog,"messages specific to the initialization part");

/* Master actor
 *
 * Schedules jobs and consolidate hosts depending on the parameters
 *
 * @param hosts: the hosts to manage
 * @param scheduling_policy: the scheduling policy chosen
 * @param conso_policy: the consolidation policy chosen
 * @param jobs_file: a file containing the jobs to schedule
 *
 * job file look like that:
 * ARRIVAL_job1 CORES_job1 DURATION_job1
 * ARRIVAL_job2 CORES_job2 DURATION_job2
 * etc...
 */
void master_fog(std::vector<simgrid::s4u::Host*> hosts, int scheduling_policy, int green_producers, double latency_range, std::string jobs_file)
{

    std::map<int, std::vector<simgrid::s4u::Host*>> hosts_per_district = GreenHost::get_hosts_per_district();
    GreenHost::create_green_production_actors(green_producers, hosts_per_district);
    simgrid::s4u::Actor::create("job_placer", hosts.front(),
                                place_jobs, jobs_file, hosts_per_district, get_weight_vector(), scheduling_policy, latency_range);

    /* Do things during simulation */
//    simgrid::s4u::Host* host9_8 = hosts_per_district[9][8];
//    simgrid::s4u::Host* host9_9 = hosts_per_district[9][9];
//    host9_8->turn_on();
//    host9_9->turn_on();
//    simgrid::s4u::this_actor::sleep_for(100);
//    simgrid::s4u::VirtualMachine* vm = new simgrid::s4u::VirtualMachine("test_vm", host9_8, 2, 2 * 1E9);
//    vm->start();
//    simgrid::s4u::this_actor::sleep_for(100);
//    sg_vm_migrate(vm, host9_9);

//    host9_6->extension<GreenHost>()->set_green_power(100);

//    simgrid::s4u::this_actor::sleep_until(200);
//    host9_6->extension<GreenHost>()->turn_on();
//    simgrid::s4u::this_actor::sleep_until(400);

//    host9_6->extension<GreenHost>()->job_reception(1, 100, 0, 1.5);

    simgrid::s4u::this_actor::sleep_until(1000);

}

/* Simulation of the system depending on the arguments given
 *
 * Initialize plugins,
 * check arguments given,
 * instantiate physical machine for the simulation,
 * launch the master actor,
 * run the simulation.
 *
 * Return a pair with the simulation engine as first
 * and the vector of physical hosts as second.
 *
 * This function must be used in the main executable and its arguments
 * are the arguments of the main function.
 */
std::pair <simgrid::s4u::Engine*, std::vector<simgrid::s4u::Host*>> simulation_fog(int argc, char* argv[])
{
    static simgrid::s4u::Engine e(&argc, argv);
    plugins_init();
    XBT_VERB("Init simulation engine");
    argument_checker(argc,argv);
    std::string platform_file = argv[1];
    std::string config_file = argv[2];
    std::string jobs_file = argv[3];
    std::string log_folder = argv[4];

    e.load_platform(argv[1]);
    std::vector<double> params = config_parameters(argv[2]);
    int init_state =           params[0];
    int scheduling_policy =    params[1];
    int consolidation_policy = params[2];
    int green_producers =      params[3];
    double latency_range =     params[4];
    double ratio =             params[5];
    double threshold =         params[6];

    std::vector<simgrid::s4u::Host*> hosts = e.get_all_hosts();
//    int start_log = 0;
//    int end_log = 100;
    int start_log = 86400;
    int end_log = 259200;
    init_hosts(init_state, start_log);
    create_logs(log_folder, platform_file, config_file, jobs_file);
    signals_connection(log_folder, hosts, start_log, end_log);
    apply_consolidation_policy(consolidation_policy, ratio, threshold);
    GreenHost::init_global_values(start_log);

    XBT_VERB("Instantiating master on primary host");
    simgrid::s4u::Actor::create("master", hosts.front(), master_fog, hosts, scheduling_policy, green_producers, latency_range, jobs_file);

    XBT_INFO("Running simulation...");
    e.run();

    std::pair <simgrid::s4u::Engine*, std::vector<simgrid::s4u::Host*>> p (e.get_instance(), hosts);
    return p;
}
