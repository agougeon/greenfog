#include "simgrid/s4u.hpp"
#include "simgrid/plugins/energy.h"
#include "simgrid/s4u/VirtualMachine.hpp"
#include "simgrid/plugins/live_migration.h"

#include "scheduling.hpp"
#include "consolidation.hpp"
#include "green_host.hpp"
#include "datacenter.hpp"
#include "tools.hpp"

#include <fstream>


XBT_LOG_NEW_DEFAULT_CATEGORY(initialization_dc,"messages specific to the initialization part");

/* Master actor
 *
 * Schedules jobs and consolidate hosts depending on the parameters
 *
 * @param hosts: the hosts to manage
 * @param scheduling_policy: the scheduling policy chosen
 * @param conso_policy: the consolidation policy chosen
 * @param jobs_file: a file containing the jobs to schedule
 *
 * ATM the job file look like that:
 * ARRIVAL_job1 CORES_job1 DURATION_job1
 * ARRIVAL_job2 CORES_job2 DURATION_job2
 * etc...
 */
void master_dc(std::vector<DataCenter*> dcs, int scheduling_policy, int conso_policy, std::string jobs_file)
{
    //~ for (int i = 0; i < dcs.size(); i++)
    //~ {
    //~ simgrid::s4u::Actor::create("pv_production_" + dcs[i]->get_city(), dcs[i]->get_hosts().front(),
    //~ pv_production_actor, "../photo-voltaic/trace-" + std::to_string(i+1) + ".csv", dcs[i]);
    //~ }

    int nb_jobs = 0;
    std::string line;
    std::ifstream file(jobs_file);
    while (getline(file, line))
        nb_jobs++;

    std::ifstream infile(jobs_file);
    std::vector<std::array<int, 3>> jobs;
    std::array<int, 3> job {0, 0, 0};
    int job_id = 0;
    int arrival, cores;
    double duration;
    int current_time = 0;
    while (infile >> arrival >> cores >> duration)
    {
        XBT_VERB("Scheduling job %d/%d: %d %d %f", job_id++ + 1, nb_jobs, arrival, cores, duration);
        job[0] = arrival;
        job[1] = cores;
        job[2] = duration;
        if (arrival > current_time)
        {
            schedule_jobs(dcs, jobs, scheduling_policy);
            jobs.clear();
            simgrid::s4u::this_actor::sleep_until(arrival);
            current_time = arrival;
            consolidate(dcs, conso_policy);
        }
        jobs.push_back(job);
    }
    schedule_jobs(dcs, jobs, scheduling_policy);
    consolidate(dcs, conso_policy);

    //~ simgrid::s4u::this_actor::sleep_for(10);
    //~ dcs[1]->get_hosts()[1]->extension<GreenHost>()->turn_off();
    //~ dcs[1]->get_hosts()[2]->extension<GreenHost>()->turn_off();
    //~ simgrid::s4u::this_actor::sleep_for(20);
    //~ dcs[1]->get_hosts()[1]->extension<GreenHost>()->turn_on();
    //~ simgrid::s4u::this_actor::sleep_for(160);
    //~ dcs[1]->get_hosts()[1]->extension<GreenHost>()->create_vm("test", 1, 10);
    //~ simgrid::s4u::this_actor::sleep_for(20);

}

/* Simulation of the system depending on the arguments given
 *
 * Initialize plugins,
 * check arguments given,
 * instantiate physical machine for the simulation,
 * launch the master actor,
 * run the simulation.
 *
 * Return a pair with the simulation engine as first
 * and the vector of physical hosts as second.
 *
 * This function must be used in the main executable and its arguments
 * are the arguments of the main function.
 */
std::pair <simgrid::s4u::Engine*, std::vector<simgrid::s4u::Host*>> simulation_dc(int argc, char* argv[])
{
    plugins_init();
    XBT_VERB("Init simulation engine");
    static simgrid::s4u::Engine e(&argc, argv);

    XBT_VERB("Checking arguments");
    xbt_assert(argc > 3, "Usage: %s platform_file.xml config_file.txt jobs_file.txt: %s msg_platform.xml config.txt jobs.txt\n", argv[0], argv[0]);
    e.load_platform(argv[1]);

    XBT_VERB("Parsing config file");
    std::ifstream infile(argv[2]);
    int init_state, scheduling_policy, consolidation_policy;
    while (infile >> init_state >> scheduling_policy >> consolidation_policy){}
    XBT_VERB("Initial state: %d Scheduling policy: %d Consolidation policy: %d", init_state, scheduling_policy, consolidation_policy);

    XBT_VERB("Instantiating physical machines");
    static std::vector<simgrid::s4u::Host*> hosts = e.get_all_hosts();
    for (auto const& host:hosts)
    {
        if (!init_state)
        {
            host->turn_off();
            host->extension<GreenHost>()->set_state(GreenHost::state::OFF);
        }
    }

    std::vector<DataCenter*> dcs = split_hosts_by_datacenter(hosts);

    on_green_power_change.connect([](simgrid::s4u::Host& host)
    {
        (&host)->extension<GreenHost>()->get_dc()->update_consumed_energy();
    });

    XBT_VERB("Instantiating master on primary host");
    simgrid::s4u::Actor::create("master", hosts.front(), master_dc, dcs, scheduling_policy, consolidation_policy, argv[3]);

    XBT_INFO("Running simulation...");
    e.run();

    std::pair <simgrid::s4u::Engine*, std::vector<simgrid::s4u::Host*>> p (e.get_instance(), hosts);

    print_consumed_energy_dc(dcs);
    return p;
}
