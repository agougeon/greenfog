#ifndef GREEN_VM
#define GREEN_VM

#include "simgrid/s4u.hpp"
#include "simgrid/plugins/energy.h"
#include "simgrid/s4u/VirtualMachine.hpp"
#include "xbt/signal.hpp"

#include "green_vm.hpp"
#include "datacenter.hpp"

#include <unordered_set>

/* green host extend the class Host in simgrid by adding new
 * attributes and function.
 *
 * The extension is added to an existing host when it is created
 */

/* Initialize the plugin
 */
void sg_green_host_init();

extern simgrid::xbt::signal<void(simgrid::s4u::Host&)> on_vm_removed;
extern simgrid::xbt::signal<void(simgrid::s4u::Host&)> on_green_power_change;

class GreenHost{
public:
    static simgrid::xbt::Extension<simgrid::s4u::Host, GreenHost> EXTENSION_ID;

    explicit GreenHost(simgrid::s4u::Host* host) : host_(host), boot_time_(150) {}

    ~GreenHost() = default;

    enum class state {
        POWERING_ON,
        ON,
        EMPTYING,
        POWERING_OFF,
        OFF
    };

    /* Total energy consumed by the links before log has started
     */
    double links_energy_consumed_before_log_;

    /* Create a vm to execute a job on the host
   *
   * @param name: the name of the vm
   * @param cores: number of cores of the vm
   * @param duration: duration of the job inside the vm
   */
    simgrid::s4u::VirtualMachine* create_vm(std::string name, int cores, double duration);

    /* Add a vm to the set of vm on this host
   *
   * @param vm: the vm to add to the set
   */
    void add_vm(simgrid::s4u::VirtualMachine* vm);

    /* Remove a vm from the list of vm on this host
   *
   * @param vm: the vm to remove from the set
   */
    void remove_vm(simgrid::s4u::VirtualMachine* vm);

    /* Return the unordered set of vms on this host
   */
    std::unordered_set<simgrid::s4u::VirtualMachine*> get_vms();

    /* Migrate a vm from this host to another host
   *
   * @param vm: the vm to migrate
   * @param host: the host receiving the vm
   */
    void migrate_vm(simgrid::s4u::VirtualMachine* vm, simgrid::s4u::Host* host);

    double get_boot_time();

    /* Put the host in the ON state if possible, has no effect if the host
   * is in ON or POWERING_ON state.
   * Hosts powering on can have vms but cannot run them
   */
    void turn_on();

    /* Turn off the host
   * Fail if there is still vms on the host
   * While shutting down new vms are refused
   */
    void turn_off();

    void set_last_update(int v);

    /* Return the number of cores not used on the host
   */
    int get_spare_cores();

    /* Return true if there is no vm on the host
   */
    bool is_empty();

    /* Return the current state of the host
   *
   * See green_host.hpp for the definition of the states
   */
    GreenHost::state get_state();

    /* Set the current state of the host
   *
   * @param st: the state
   *
   * See green_host.hpp for the definition of the states
   */
    void set_state(GreenHost::state st);

    /* Return a string version of the current state of the host
   */
    std::string state_string();

    /* Return the current CPU utilization ratio of the host
   */
    double utilization();

    /* Return true if the host is underutilized
   */
    bool is_underutilized();

    /* Set the datacenter of this host
   */
    void set_dc(DataCenter* dc);

    /* Get a pointer to the datacenter of this host
   */
    DataCenter* get_dc();

    void set_green_power(double p);

    double get_green_power();

    double get_spare_green_power();

    void update_consumed_energy();

    double get_green_energy_consumed();

    double get_brown_energy_consumed();

    void add_neighboor(simgrid::s4u::Host* host, double latency);

    std::multimap<double, simgrid::s4u::Host*> get_neighbors();

    void set_checked(bool b=true);

    bool is_checked();

    void uncheck_hosts(std::vector<simgrid::s4u::Host*> hosts);

    void add_latency_initial(double l);

    std::vector<double> get_latencies_initial();

    void add_latency_final(double l);

    std::vector<double> get_latencies_final();

    void job_reception(int cores, double duration, int policy, double max_latency=0);

    std::pair<simgrid::s4u::Host*, double> find_node_latency(simgrid::s4u::Host* host, int cores, double max_latency=0);

    std::pair<simgrid::s4u::Host*, double> find_node_green_first(simgrid::s4u::Host* host, int cores, double max_latency=0);

    std::pair<simgrid::s4u::Host*, double> find_node_green_most(simgrid::s4u::Host* host, int cores, double max_latency=0);

    // called when a job end, turn off host if empty
    void consolidate();

    // called when a job end, turn off if empty and no green energy production
    void consolidate_brown();

    // return a vector containing all hosts in a latency range
    std::vector <simgrid::s4u::Host*> get_neighborhood(double range);

    // called when a job end, turn off if empty and no spare green energy
    void consolidate_ratio(double ratio, double threshold);

    // called when green energy run out
    void ask_for_green();

    void update_logs(std::vector<simgrid::s4u::Host*> hosts, std::string folder_name, int start_log, int end_log, bool force_end=false);

    void write_logs(std::string folder_name);

    static double global_power_consumption_;
    static double global_brown_power_consumption_;
    static double global_green_power_production_;
    static double global_exploitable_green_power_;
    static std::vector<simgrid::s4u::Host*> modified_consumption_hosts_;
    static double time_previous_modified_consumption_;

    /* Compute the initial global values of the class
   */
    static void init_global_values(int start_log);

    /* Return a vector containing the valid hosts (USERS hosts)
   */
    static std::vector<simgrid::s4u::Host*> get_valid_hosts();

    /* Compute a map where keys are the different district of the hosts
   * and the items are the hosts in this district
   */
    static std::map<int, std::vector<simgrid::s4u::Host*>> get_hosts_per_district();

    /* Add an actor to modify the green production of the Hosts
   * One actor per Host
   * All actors are placed on the master
   */
    static void create_green_production_actors(int green_producers, std::map<int, std::vector<simgrid::s4u::Host*>> hosts_per_district);

    void set_valid_host(bool b);

    bool is_valid_host();

    int get_message_sent();

private:

    /* Return a pointer to the host extended by the plugin
   */
    simgrid::s4u::Host* host_;

    double boot_time_;

    /* Different state in which the host can be
   */
    GreenHost::state state_ = GreenHost::state::ON;

    double boot_date_;

    /* Unordered set of vm on this host
   */
    std::unordered_set<simgrid::s4u::VirtualMachine*> vms_;

    /* Threshold on core usage under which host is considered underutilized
   */
    double underutilized_threshold_ = 0.5;

    /* The datacenter where the host is located
   */
    DataCenter* dc_ = 0;

    double last_update_ = 0;

    double last_log_time_ = -1;

    double previous_power_consumption_ = 0;

    double previous_brown_power_consumption_ = 0;

    double previous_green_power_production_ = 0;

    double green_power_ = 0;

    double green_energy_consumed_ = 0;

    double brown_energy_consumed_ = 0;

    std::multimap<double, simgrid::s4u::Host*> neighbors_;

    bool checked_ = false;

    std::vector<double> latencies_initial_;

    std::vector<double> latencies_final_;

    bool valid_host_ = true;

    int messages_sent_ = 0;

    int start_log = 86400;
};

void pv_production_actor_host(std::string file, std::vector<simgrid::s4u::Host*> hosts);
void actor_links_initial_consumption(int init_log);
#endif
