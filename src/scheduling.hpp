#ifndef SCHEDULING
#define SCHEDULING

#include "simgrid/s4u.hpp"

#include "datacenter.hpp"

/* Actor placing jobs on the hosts
 * This actor should be place on the master
 */
void place_jobs(std::string job_file, std::map<int,
                std::vector<simgrid::s4u::Host*>> hosts_per_district,
                std::vector<int> weight_vector, int scheduling_policy, double latency_range);

void schedule_jobs(std::vector<DataCenter*> dcs, std::vector<std::array<int, 3>> jobs, int policy);

/* Schedule jobs depending on the chosen policy
 *
 * @param hosts: the hosts on which jobs are placed
 * @param jobs: the jobs to place
 * @param policy: the scheduling policy chosen
 */
void schedule_jobs(std::vector<simgrid::s4u::Host*> hosts, std::vector<std::array<int, 3>>, int policy);

void round_robin(std::vector<DataCenter*> dcs, std::vector<std::array<int, 3>> jobs);

/* Schedule jobs on hosts following a round robin policy
 *
 * The first job is placed on the first host, the second job on the
 * second host, etc...
 *
 * Crash if a job cannot be scheduled
 *
 * @param hosts: the hosts on which jobs are placed
 * @param jobs: the jobs to schedule
 */
void round_robin(std::vector<simgrid::s4u::Host*> hosts, std::vector<std::array<int, 3>>);

simgrid::s4u::Host* find_host_first_fit(std::vector<DataCenter*> dcs, int cores);

/* Return a pointer to an available host with enough spare cores, 
 * following a first fit policy
 *
 * Return the first host with enough spare cores.
 *
 * Crash if there is no host available
 *
 * @param hosts: the potential hosts
 * @param cores: number of spare cores required on the host
 */
simgrid::s4u::Host* find_host_first_fit(std::vector<simgrid::s4u::Host*> hosts, int cores);

void first_fit(std::vector<DataCenter*> dcs, std::vector<std::array<int, 3>> jobs);

/* Schedule jobs on hosts following a first fit policy
 *
 * Jobs are placed on the first host that can contain it
 *
 * @param hosts: the hosts on which jobs are placed
 * @param jobs: the jobs to schedule
 */
void first_fit(std::vector<simgrid::s4u::Host*> hosts, std::vector<std::array<int, 3>>);

simgrid::s4u::Host* find_host_first_fit_optimized(std::vector<DataCenter*> dcs, int cores);

/* Return a pointer to an available host with enough spare cores, 
 * following an optimized first fit policy.
 *
 * It searches for a ON or POWERING_ON host, then if nothing found look
 * for an EMPTYING host, and finally look for a OFF host
 *
 * Crash if there is no host available
 *
 * @param hosts: the potential hosts
 * @param cores: number of spare cores required on the host
 */
simgrid::s4u::Host* find_host_first_fit_optimized(std::vector<simgrid::s4u::Host*> hosts, int cores);

void first_fit_optimized(std::vector<DataCenter*> dcs, std::vector<std::array<int, 3>> jobs);

/* Schedule jobs on hosts following an optimized first fit policy
 *
 * Jobs are placed on the first available host, but avoid turning on OFF
 * hosts if there is place on ON / POWERING _ON / EMPTYING hosts.
 *
 * @param hosts: the hosts on which jobs are placed
 * @param jobs: the jobs to schedule
 */
void first_fit_optimized(std::vector<simgrid::s4u::Host*> hosts, std::vector<std::array<int, 3>>);

/* Return a pointer to an available host with enough spare cores, 
 * following an mbfd policy.
 *
 * It searches for a ON or POWERING_ON host, then if nothing found look
 * for an EMPTYING host, and finally look for a OFF host.
 * Consider the utilization ratio of the host and select an host with
 * the highest utilization, thus reducing the overhead on energy
 * consumption.
 *
 * Crash if there is no host available
 *
 * @param hosts: the potential hosts
 * @param cores: number of spare cores required on the host
 */
simgrid::s4u::Host* find_host_mbfd(std::vector<simgrid::s4u::Host*> hosts, int cores);

void mbfd(std::vector<DataCenter*> dcs, std::vector<std::array<int, 3>> jobs);

/* Schedule jobs on hosts following a mbfd policy
 *
 * The placement of the jobs minimize the overhead on energy consumption
 *
 * @param hosts: the hosts on which jobs are placed
 * @param jobs: the jobs to schedule
 */
void mbfd(std::vector<simgrid::s4u::Host*> hosts, std::vector<std::array<int, 3>>);

void ood_mare(std::vector<DataCenter*> dcs, std::vector<std::array<int, 3>>);

#endif
