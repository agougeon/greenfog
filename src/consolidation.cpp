#include "simgrid/plugins/live_migration.h"

#include "consolidation.hpp"
#include "green_host.hpp"
#include "scheduling.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(consolidation,"messages specific to the consolidation part");

void consolidate(std::vector<DataCenter*> dcs, int conso_policy)
{
    for (auto const& dc: dcs)
    {
        consolidate(dc->get_hosts(), conso_policy);
    }
}


/* Apply consolidation on the hosts, depending on the chosen policy
 *
 * @param hosts: the vector of hosts to consolidate
 * @param conso_policy: the consolidation policy
 *
 * (0 = no consolidation,
 *  1 = consolidation using first fit optimized to place vms,
 *  2 = consolidation using mbfd to place vms)
 *
 * Consolidation only consider underutilized hosts for the moment
 */
void consolidate(std::vector<simgrid::s4u::Host*> hosts, int conso_policy)
{
    switch (conso_policy) {
    case 0: break;
    case 1: close_underutilized_hosts_first_fit_optimized(hosts);
        break;
    case 2: close_underutilized_hosts_mbfd(hosts);
        break;
    default : XBT_INFO("consolidate: bad consolidation policy.");
        exit(0);
        break;
    }
}

/* Consolidate hosts by shutting down empty hosts and use the first fit 
 * optimized policy to move vms from underutilized hosts.
 *
 * @param hosts: the vector of hosts to consolidate
 */
void close_underutilized_hosts_first_fit_optimized(std::vector<simgrid::s4u::Host*> hosts)
{
    std::vector<simgrid::s4u::VirtualMachine*> vms_to_migrate;
    simgrid::s4u::Host* new_host;

    // starts at 1 because the master host should not be switched off
    for (int i = 1; i < hosts.size(); i++)
    {
        if (hosts[i]->extension<GreenHost>()->get_state() == GreenHost::state::ON && hosts[i]->extension<GreenHost>()->is_underutilized())
        {
            if (hosts[i]->extension<GreenHost>()->is_empty())
            {
                hosts[i]->extension<GreenHost>()->turn_off();
            }
            else
            {
                hosts[i]->extension<GreenHost>()->set_state(GreenHost::state::EMPTYING);
                for (auto const& vm: hosts[i]->extension<GreenHost>()->get_vms())
                {
                    if (!sg_vm_is_migrating(vm))
                        vms_to_migrate.push_back(vm);
                }
            }
        }
    }
    for (auto const& vm: vms_to_migrate)
    {
        new_host = find_host_first_fit_optimized(hosts, vm->get_core_count());
        if (new_host != vm->get_pm())
            vm->get_pm()->extension<GreenHost>()->migrate_vm(vm, new_host);
    }
}

/* Consolidate hosts by shutting down empty hosts and use the mbfd 
 * policy to move vms from underutilized hosts.
 *
 * @param hosts: the vector of hosts to consolidate
 */
void close_underutilized_hosts_mbfd(std::vector<simgrid::s4u::Host*> hosts)
{
    std::vector<simgrid::s4u::VirtualMachine*> vms_to_migrate;
    simgrid::s4u::Host* new_host;

    // starts at 1 because the master host should not be switched off
    // turn off empty hosts and built the vector of vm to migrate
    for (int i = 1; i < hosts.size(); i++)
    {
        if (hosts[i]->extension<GreenHost>()->get_state() == GreenHost::state::ON && hosts[i]->extension<GreenHost>()->is_underutilized())
        {
            if (hosts[i]->extension<GreenHost>()->is_empty())
            {
                hosts[i]->extension<GreenHost>()->turn_off();
            }
            else
            {
                hosts[i]->extension<GreenHost>()->set_state(GreenHost::state::EMPTYING);
                for (auto const& vm: hosts[i]->extension<GreenHost>()->get_vms())
                {
                    if (!sg_vm_is_migrating(vm))
                        vms_to_migrate.push_back(vm);
                }
            }
        }
    }
    // sort vms by decreasing CPU utilization
    std::sort(vms_to_migrate.begin(), vms_to_migrate.end(), [](simgrid::s4u::VirtualMachine* vm1, simgrid::s4u::VirtualMachine* vm2)
    {
        return vm1->get_core_count() > vm2->get_core_count();
    });
    // migrate vms on new hosts
    for (auto const& vm: vms_to_migrate)
    {
        new_host = find_host_mbfd(hosts, vm->get_core_count());
        if (new_host != vm->get_pm())
            vm->get_pm()->extension<GreenHost>()->migrate_vm(vm, new_host);
    }
}


// ------------- DEPRECATED -----------


/* Actor used to empty a host: stay on a host and keep emptying it as long 
 * as it is in EMPTYING state, when empty turn off the host
 * Used in close_underutilized_hosts
 * DO NOT USE DIRECTLY
 */
void closing_actor(std::vector<simgrid::s4u::Host*> hosts)
{
    int clean_frequency = 10;
    simgrid::s4u::Host* current_host = simgrid::s4u::this_actor::get_host();
    simgrid::s4u::Host* new_host;

    while ((current_host->extension<GreenHost>()->get_state() == GreenHost::state::EMPTYING) && !(current_host->extension<GreenHost>()->is_empty()))
    {
        for (auto const& vm: current_host->extension<GreenHost>()->get_vms())
        {
            if (!sg_vm_is_migrating(vm))
            {
                new_host = find_host_first_fit(hosts, vm->get_core_count());
                if (new_host != 0 && new_host != current_host)
                    current_host->extension<GreenHost>()->migrate_vm(vm, new_host);
            }
        }
        simgrid::s4u::this_actor::sleep_for(clean_frequency);
    }
    if (current_host->extension<GreenHost>()->get_state() == GreenHost::state::EMPTYING)
    {
        current_host->extension<GreenHost>()->turn_off();
    }
}

/* deprecated for now
 */
void consolidation_actor(std::vector<simgrid::s4u::Host*> hosts, int conso_policy, int period)
{
    switch (conso_policy)
    {
    case 1:
        while(true)
        {
            simgrid::s4u::this_actor::sleep_for(period);
            close_underutilized_hosts_first_fit_optimized(hosts);
        }
    case 2:
        while(true)
        {
            simgrid::s4u::this_actor::sleep_for(period);
            close_underutilized_hosts_mbfd(hosts);
        }
    }
}

