#ifndef DATACENTER
#define DATACENTER

#include "simgrid/s4u.hpp"


class DataCenter{
public:
    DataCenter(std::string city, std::vector<simgrid::s4u::Host*> hosts);

    std::string get_city();

    std::vector<simgrid::s4u::Host*> get_hosts();

    void set_power_consumption(double power);

    double get_power_consumption();

    void set_green_power_production(double energy);

    double get_green_power_available();

    double get_green_energy_consumed();

    double get_brown_energy_consumed();

    void update_consumed_energy();

private:
    std::string city_;

    std::vector<simgrid::s4u::Host*> hosts_;

    double power_consumption_ = 0;

    double green_power_production_ = 1000000;

    double green_energy_consumed_ = 0;

    double brown_energy_consumed_ = 0;

    double last_update_ = 0;
};

void pv_production_actor(std::string file, DataCenter* dc);


#endif
