#include "simgrid/s4u/VirtualMachine.hpp"

#include "green_vm.hpp"
#include "green_host.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(green_vm, "messages specific to the green vm extension");

simgrid::xbt::Extension<simgrid::s4u::Host, GreenVm> GreenVm::EXTENSION_ID;

/* green vm extend the class VirtualMachine in simgrid by adding new
 * attributes and function.
 *
 * The extension is added to an existing vm when it is started
 * (and not when it is created).
 */

static void on_creation(simgrid::s4u::Host& host)
{
    if(simgrid::s4u::VirtualMachine* vm = dynamic_cast<simgrid::s4u::VirtualMachine*>(&host))
        vm->extension_set(new GreenVm(vm));
}


/* Initialize the plugin
 */
void sg_green_vm_init(){
    if(GreenVm::EXTENSION_ID.valid())
        return;

    GreenVm::EXTENSION_ID = simgrid::s4u::Host::extension_create<GreenVm>();
    simgrid::s4u::Host::on_creation.connect(&on_creation);
}

/* Start the job inside the vm (vm will be killed at the end of the duration)
 *
 * @param vm: the vm in which we want to its job
 * @param duration: duration of the job inside the vm
 */
void sg_green_vm_start(simgrid::s4u::VirtualMachine* vm, double duration){
    vm->extension<GreenVm>()->start_computing(duration);
}

/* Return the remaining duration of the job inside the vm
 *
 * @param vm: the vm
 */
double sg_green_vm_get_remaining_duration(simgrid::s4u::VirtualMachine* vm){
    return vm->extension<GreenVm>()->get_remaining_duration();
}

/* Return the time when the vm was started
 */
double GreenVm::get_start_time(){
    return start_time_;
}

/* Set the start time of the vm
 *
 * @param time: the time to set
 */
void GreenVm::set_start_time(double time){
    start_time_ = time;
}

/* Return the remaining duration of the job inside the vm
 */
double GreenVm::get_remaining_duration(){
    return (start_time_<0)? -1 : start_time_ + duration_ - simgrid::s4u::Engine::get_clock();
}

/* Actor used to control the lifetime of the vm
 *
 * Used in start_computing()
 * DO NOT USE DIRECLTY
 */
static void life_cycle_manager(){
    simgrid::s4u::VirtualMachine* vm = dynamic_cast<simgrid::s4u::VirtualMachine*>(simgrid::s4u::this_actor::get_host());

    simgrid::s4u::this_actor::execute(1);
    vm->extension<GreenVm>()->set_start_time(simgrid::s4u::Engine::get_clock());
    for(int i=0;i<vm->get_core_count();i++)
        simgrid::s4u::this_actor::exec_async(std::numeric_limits<double>::max());

    simgrid::s4u::this_actor::sleep_for(vm->extension<GreenVm>()->get_remaining_duration());
    vm->get_pm()->extension<GreenHost>()->remove_vm(vm);
    simgrid::s4u::this_actor::set_host(vm->get_pm());
    vm->destroy();
}

/* Begin the computing of the job in the vm
 *
 * @param duration: duration of the job inside the vm
 */
void GreenVm::start_computing(double duration){
    duration_ = duration;
    simgrid::s4u::ActorPtr manager = simgrid::s4u::Actor::create("life_cycle_manager", vm_, life_cycle_manager);
    manager->daemonize();
}

void GreenVm::set_reception_host(simgrid::s4u::Host* h)
{
    reception_host_ = h;
}

simgrid::s4u::Host* GreenVm::get_reception_host()
{
    return reception_host_;
}

void GreenVm::set_latency_bound(double l)
{
    latency_bound_ = l;
}

double GreenVm::get_latency_bound()
{
    return latency_bound_;
}

void GreenVm::set_latency_initial(double l)
{
    latency_initial_ = l;
}

double GreenVm::get_latency_initial()
{
    return latency_initial_;
}

void GreenVm::set_latency_current(double l)
{
    latency_current_ = l;
}

double GreenVm::get_latency_current()
{
    return latency_current_;
}
