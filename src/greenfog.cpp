#include "simgrid/s4u.hpp"

#include "initialization_fog.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(main,"messages specific to the main part");

int main(int argc, char* argv[]) {
    simulation_fog(argc, argv);

    // --log=greenfog.threshold:verbose
    return 0;
}


