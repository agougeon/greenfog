#include "simgrid/s4u.hpp"

/* Task with very high flops, MUST be killed
 */
void infinite_task()
{
    simgrid::s4u::this_actor::execute(std::numeric_limits<double>::max());
}

/* Task to do for an actor
 *
 * @param w: flops to do
 */
void task(int w)
{
    simgrid::s4u::this_actor::execute(w);
}

/* Task executing only 1 flop
 */
void unit_task()
{
    simgrid::s4u::this_actor::execute(1);
}
