#include "simgrid/s4u.hpp"
#include "simgrid/plugins/live_migration.h"

#include "green_host.hpp"
#include "scheduling.hpp"
#include "tools.hpp"

#include <fstream>

XBT_LOG_NEW_DEFAULT_CATEGORY(scheduling,"messages specific to the scheduling part");

void place_jobs(std::string jobs_file, std::map<int, std::vector<simgrid::s4u::Host*>> hosts_per_district, std::vector<int> weight_vector, int scheduling_policy, double latency_range) {
    XBT_VERB("place_jobs: start");
    std::ifstream jobsfile(jobs_file);
    int arrival, cores;
    double duration;
    std::vector<simgrid::s4u::Host*> possible_hosts;
    simgrid::s4u::Host* selected_host;
    srand (time(NULL));
    XBT_VERB("place_jobs: reading jobs from: %s", jobs_file.c_str());
    while (jobsfile >> arrival >> cores >> duration)
    {
        simgrid::s4u::this_actor::sleep_until(arrival);
        possible_hosts = hosts_per_district[weight_vector[rand() % weight_vector.size()]];
        selected_host = possible_hosts[rand() % possible_hosts.size()];
        selected_host->extension<GreenHost>()->job_reception(cores, duration, scheduling_policy, latency_range);
    }
}

void schedule_jobs(std::vector<DataCenter*> dcs, std::vector<std::array<int, 3>> jobs, int policy)
{
    switch (policy) {
    case 0: round_robin(dcs, jobs);
        break;
    case 1: first_fit(dcs, jobs);
        break;
    case 2: first_fit_optimized(dcs, jobs);
        break;
    case 3: mbfd(dcs, jobs);
        break;
    case 4: ood_mare(dcs, jobs);
        break;
    default: XBT_INFO("schedule_jobs: bad scheduling policy.");
        exit(0);
        break;
    }
}

/* Schedule jobs depending on the chosen policy
 *
 * @param hosts: the hosts on which jobs are placed
 * @param jobs: the jobs to place
 * @param policy: the scheduling policy chosen
 */
void schedule_jobs(std::vector<simgrid::s4u::Host*> hosts, std::vector<std::array<int, 3>> jobs, int policy)
{
    switch (policy) {
    case 0: round_robin(hosts, jobs);
        break;
    case 1: first_fit(hosts, jobs);
        break;
    case 2: first_fit_optimized(hosts, jobs);
        break;
    case 3: mbfd(hosts, jobs);
        break;
    default: XBT_INFO("schedule_jobs: bad scheduling policy.");
        exit(0);
        break;
    }
}

void round_robin(std::vector<DataCenter*> dcs, std::vector<std::array<int, 3>> jobs)
{
    static int mod = 0;
    for (auto const& job: jobs)
    {
        first_fit(dcs[mod++ % dcs.size()]->get_hosts(), std::vector<std::array<int, 3>> {job});
    }
}

/* Schedule jobs on hosts following a round robin policy
 *
 * The first job is placed on the first host, the second job on the
 * second host, etc...
 *
 * Crash if a job cannot be scheduled
 *
 * @param hosts: the hosts on which jobs are placed
 * @param jobs: the jobs to schedule
 */
void round_robin(std::vector<simgrid::s4u::Host*> hosts, std::vector<std::array<int, 3>> jobs)
{
    static int mod = 0;
    simgrid::s4u::Host* host;
    GreenHost::state host_state;
    int id = 0;

    for (auto const& job: jobs)
    {
        host = hosts[mod++ % hosts.size()];
        host_state = host->extension<GreenHost>()->get_state();
        if (host->extension<GreenHost>()->get_spare_cores() >= job[1])
        {
            if (host_state == GreenHost::state::ON || host_state == GreenHost::state::POWERING_ON)
            {
                host->extension<GreenHost>()->create_vm("job_" + std::to_string(job[0]) + " " + std::to_string(id++), job[1], job[2]);
            }
            else if (host_state == GreenHost::state::OFF)
            {
                host->extension<GreenHost>()->turn_on();
                host->extension<GreenHost>()->create_vm("job_" + std::to_string(job[0]) + " " + std::to_string(id++), job[1], job[2]);
            }
            else
            {
                XBT_INFO("round_robin: wrong state to receive a job (must be either ON or POWERING_ON).");
                exit(0);
            }
        }
        else
        {
            XBT_INFO("round_robin: designated host cannot receive the job.");
            exit(0);
        }
    }
}

simgrid::s4u::Host* find_host_first_fit(std::vector<DataCenter*> dcs, int cores)
{
    simgrid::s4u::Host* host = 0;
    for (auto const& dc: dcs)
    {
        host = find_host_first_fit(dc->get_hosts(), cores);
        if (host != 0)
            return host;
    }
    return 0;
}

/* Return a pointer to an available host with enough spare cores, 
 * following a first fit policy
 *
 * Return the first host with enough spare cores.
 *
 * Crash if there is no host available
 *
 * @param hosts: the potential hosts
 * @param cores: number of spare cores required on the host
 */
simgrid::s4u::Host* find_host_first_fit(std::vector<simgrid::s4u::Host*> hosts, int cores)
{
    for (auto const& host: hosts)
    {
        if (host->extension<GreenHost>()->get_spare_cores() >= cores
                && host->extension<GreenHost>()->get_state() != GreenHost::state::POWERING_OFF)
        {
            host->extension<GreenHost>()->turn_on();
            return host;
        }
    }
    return 0;
}

void first_fit(std::vector<DataCenter*> dcs, std::vector<std::array<int, 3>> jobs)
{
    simgrid::s4u::Host* host = 0;
    int id = 0;

    for (auto const& job: jobs)
    {
        for (auto const& dc: dcs)
        {
            host = find_host_first_fit(dc->get_hosts(), job[1]);
            if (host != 0)
            {
                host->extension<GreenHost>()->create_vm("job_" + std::to_string(job[0]) + " " + std::to_string(id++), job[1], job[2]);
                break;
            }
        }
        if (host == 0)
        {
            XBT_INFO("first_fit: no suitable host has been found among the datacenters.");
            exit(0);
        }
    }
}


/* Schedule jobs on hosts following a first fit policy
 *
 * Jobs are placed on the first host that can contain it
 *
 * @param hosts: the hosts on which jobs are placed
 * @param jobs: the jobs to schedule
 */
void first_fit(std::vector<simgrid::s4u::Host*> hosts, std::vector<std::array<int, 3>> jobs)
{
    simgrid::s4u::Host* host = 0;
    static int id = 0;

    for (auto const& job:jobs)
    {
        host = find_host_first_fit(hosts, job[1]);
        if (host != 0)
            host->extension<GreenHost>()->create_vm("job_" + std::to_string(job[0]) + " " + std::to_string(id++), job[1], job[2]);
        else
        {
            XBT_INFO("first_fit: no suitable host has been found for the job.");
            exit(0);
        }
    }
}

simgrid::s4u::Host* find_host_first_fit_optimized(std::vector<DataCenter*> dcs, int cores)
{
    simgrid::s4u::Host* host = 0;
    for (auto const& dc: dcs)
    {
        host = find_host_first_fit_optimized(dc->get_hosts(), cores);
        if (host != 0)
            return host;
    }
    return 0;
}


/* Return a pointer to an available host with enough spare cores, 
 * following an optimized first fit policy.
 *
 * It searches for a ON or POWERING_ON host, then if nothing found look
 * for an EMPTYING host, and finally look for a OFF host
 *
 * Crash if there is no host available
 *
 * @param hosts: the potential hosts
 * @param cores: number of spare cores required on the host
 */
simgrid::s4u::Host* find_host_first_fit_optimized(std::vector<simgrid::s4u::Host*> hosts, int cores)
{
    GreenHost::state host_state;
    simgrid::s4u::Host* new_host = 0;
    GreenHost::state new_host_state;
    int i = 0;

    while (!(new_host_state == GreenHost::state::ON || new_host_state == GreenHost::state::POWERING_ON)
           && i < hosts.size())
    {
        host_state = hosts[i]->extension<GreenHost>()->get_state();
        if (hosts[i]->extension<GreenHost>()->get_spare_cores() >= cores && host_state != GreenHost::state::POWERING_OFF)
        {
            if (host_state == GreenHost::state::ON
                    || host_state == GreenHost::state::POWERING_ON
                    || (host_state == GreenHost::state::EMPTYING && new_host_state == GreenHost::state::OFF)
                    || (host_state == GreenHost::state::OFF && !new_host))
                new_host = hosts[i];
            new_host_state = host_state;
        }
        i++;
    }
    if (new_host)
    {
        new_host->extension<GreenHost>()->turn_on();
        return new_host;
    }
    return 0;
}

void first_fit_optimized(std::vector<DataCenter*> dcs, std::vector<std::array<int, 3>> jobs)
{
    simgrid::s4u::Host* host = 0;
    int id = 0;

    for (auto const& job: jobs)
    {
        for (auto const& dc: dcs)
        {
            host = find_host_first_fit_optimized(dc->get_hosts(), job[1]);
            if (host != 0)
            {
                host->extension<GreenHost>()->create_vm("job_" + std::to_string(job[0]) + " " + std::to_string(id++), job[1], job[2]);
                break;
            }
        }
        if (host == 0)
        {
            XBT_INFO("first_fit_optimized: no suitable host has been found among the datacenters.");
            exit(0);
        }
    }
}

/* Schedule jobs on hosts following an optimized first fit policy
 *
 * Jobs are placed on the first available host, but avoid turning on OFF
 * hosts if there is place on ON / POWERING _ON / EMPTYING hosts.
 *
 * @param hosts: the hosts on which jobs are placed
 * @param jobs: the jobs to schedule
 */
void first_fit_optimized(std::vector<simgrid::s4u::Host*> hosts, std::vector<std::array<int, 3>> jobs)
{
    simgrid::s4u::Host* host = 0;
    int id = 0;

    for (auto const& job:jobs)
    {
        host = find_host_first_fit_optimized(hosts, job[1]);
        if (host != 0)
            host->extension<GreenHost>()->create_vm("job_" + std::to_string(job[0]) + " " + std::to_string(id++), job[1], job[2]);
        else
        {
            XBT_INFO("first_fit_optimized: no suitable host has been found for the job.");
            exit(0);
        }
    }
}

/* Return a pointer to an available host with enough spare cores, 
 * following an mbfd policy.
 *
 * It searches for a ON or POWERING_ON host, then if nothing found look
 * for an EMPTYING host, and finally look for a OFF host.
 * Consider the utilization ratio of the host and select an host with
 * the highest utilization, thus reducing the overhead on energy
 * consumption.
 *
 * Crash if there is no host available
 *
 * @param hosts: the potential hosts
 * @param cores: number of spare cores required on the host
 */
simgrid::s4u::Host* find_host_mbfd(std::vector<simgrid::s4u::Host*> hosts, int cores)
{
    GreenHost::state host_state;
    double host_utilization = 0;
    simgrid::s4u::Host* new_host = 0;
    GreenHost::state new_host_state;
    double highest_utilization = -1;

    for (auto const& host: hosts)
    {
        host_state = host->extension<GreenHost>()->get_state();
        host_utilization = host->extension<GreenHost>()->utilization();
        if (host->extension<GreenHost>()->get_spare_cores() >= cores && host_state != GreenHost::state::POWERING_OFF)
        {
            if (host_utilization > highest_utilization)
            {
                new_host = host;
                highest_utilization = host_utilization;
                new_host_state = host_state;
            }
            else if (host_utilization == highest_utilization)
            {
                if ((new_host_state == GreenHost::state::OFF && host_state != GreenHost::state::OFF)
                        || (new_host_state == GreenHost::state::EMPTYING && (host_state == GreenHost::state::ON || host_state == GreenHost::state::POWERING_ON)))
                {
                    new_host = host;
                    new_host_state = host_state;
                }
            }
        }
    }
    if (new_host)
    {
        new_host->extension<GreenHost>()->turn_on();
        return new_host;
    }
    return 0;
}

void mbfd(std::vector<DataCenter*> dcs, std::vector<std::array<int, 3>> jobs)
{
    simgrid::s4u::Host* host = 0;
    int id = 0;

    std::sort(jobs.begin(), jobs.end(), [](std::array<int, 3> job1 , std::array<int, 3> job2)
    {
        return job1[1] > job2[1];
    });

    std::vector<simgrid::s4u::Host*> hosts;
    for (auto const& dc: dcs)
    {
        for (auto const& host : dc->get_hosts())
        {
            hosts.push_back(host);
        }
    }

    for (auto const& job: jobs)
    {
        host = find_host_mbfd(hosts, job[1]);
        if (host != 0)
            host->extension<GreenHost>()->create_vm("job_" + std::to_string(job[0]) + " " + std::to_string(id++), job[1], job[2]);
        else
        {
            XBT_INFO("mbfd: no suitable host has been found for the job.");
            exit(0);
        }
    }
}

/* Schedule jobs on hosts following a mbfd policy
 *
 * The placement of the jobs minimize the overhead on energy consumption
 *
 * @param hosts: the hosts on which jobs are placed
 * @param jobs: the jobs to schedule
 */
void mbfd(std::vector<simgrid::s4u::Host*> hosts, std::vector<std::array<int, 3>> jobs)
{
    simgrid::s4u::Host* host = 0;
    int id = 0;

    std::sort(jobs.begin(), jobs.end(), [](std::array<int, 3> job1 , std::array<int, 3> job2)
    {
        return job1[1] > job2[1];
    });

    for (auto const& job: jobs)
    {
        host = find_host_mbfd(hosts, job[1]);
        if (host != 0)
            host->extension<GreenHost>()->create_vm("job_" + std::to_string(job[0]) + " " + std::to_string(id++), job[1], job[2]);
        else
        {
            XBT_INFO("mbfd: no suitable host has been found for the job.");
            exit(0);
        }
    }
}

simgrid::s4u::Host* find_host_ood_mare(DataCenter* dc, int cores)
{
    GreenHost::state host_state;
    int spare_cores = 0;
    for (auto const& host: dc->get_hosts())
    {
        host_state = host->extension<GreenHost>()->get_state();
        spare_cores = host->extension<GreenHost>()->get_spare_cores();
        if (spare_cores >= cores && host_state != GreenHost::state::POWERING_OFF)
        {
            int wattmin = sg_host_get_wattmin_at(host, 0);
            int wattmax = sg_host_get_wattmax_at(host, 0);
            int cores_used = host->get_core_count() - (spare_cores - cores);
            double new_conso = wattmin + (cores_used - 1) * (wattmax - wattmin) / (host->get_core_count()-1);
            dc->set_power_consumption(dc->get_power_consumption() - sg_host_get_current_consumption(host) + new_conso);

            host->extension<GreenHost>()->turn_on();
            return host;
        }
    }
    return 0;
}

/* TO TEST
 */
void ood_mare(std::vector<DataCenter*> dcs, std::vector<std::array<int, 3>> jobs)
{
    simgrid::s4u::Host* host = 0;
    int id = 0;

    std::sort(dcs.begin(), dcs.end(), [](DataCenter* a, DataCenter* b)
    {
        return a->get_green_power_available() > b->get_green_power_available();
    });

    int i;
    for (auto const& job: jobs)
    {
        if (dcs[0]->get_green_power_available() < dcs[1]->get_green_power_available())
        {
            std::sort(dcs.begin(), dcs.end(), [](DataCenter* a, DataCenter* b)
            {
                return a->get_green_power_available() > b->get_green_power_available();
            });
        }

        //~ host = find_host_first_fit(hosts_by_city[green_prod_by_city[i].first], job[1]);
        i++;
        if (host != 0)
            host->extension<GreenHost>()->create_vm("job_" + std::to_string(job[0]) + " " + std::to_string(id++), job[1], job[2]);
        else
        {
            XBT_INFO("ood_mare: no suitable host has been found for the job.");
            exit(0);
        }
    }
}
