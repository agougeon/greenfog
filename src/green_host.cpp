#include "simgrid/s4u/VirtualMachine.hpp"
#include "simgrid/plugins/live_migration.h"
#include "simgrid/plugins/energy.h"

#include "green_host.hpp"
#include "green_vm.hpp"
#include "tasks.hpp"
#include "tools.hpp"

#include <fstream>
#include <math.h>

XBT_LOG_NEW_DEFAULT_CATEGORY(green_host, "messages specific to the green host extension");

simgrid::xbt::signal<void(simgrid::s4u::Host&)> on_vm_removed;
simgrid::xbt::signal<void(simgrid::s4u::Host&)> on_green_power_change;

simgrid::xbt::Extension<simgrid::s4u::Host, GreenHost> GreenHost::EXTENSION_ID;

/* green host extend the class Host in simgrid by adding new
 * attributes and function.
 *
 * The extension is added to an existing host when it is created
 */

static void on_creation(simgrid::s4u::Host& host)
{
    if (dynamic_cast<simgrid::s4u::VirtualMachine*>(&host)) // Ignore virtual machines
        return;

    host.extension_set(new GreenHost(&host));
}

/* Initialize the plugin
 */
void sg_green_host_init(){
    if(GreenHost::EXTENSION_ID.valid())
        return;

    GreenHost::EXTENSION_ID = simgrid::s4u::Host::extension_create<GreenHost>();

    if(simgrid::s4u::Engine::is_initialized()){
        simgrid::s4u::Engine *e = simgrid::s4u::Engine::get_instance();
        for(auto& host : e->get_all_hosts()){
            if(!(dynamic_cast<simgrid::s4u::VirtualMachine*>(host)))
                host->extension_set(new GreenHost(host));
        }
    }
    simgrid::s4u::Host::on_creation.connect(&on_creation);
}

/* Total energy consumed by the links before log has started
 */
double links_energy_consumed_before_log_ = 0;


/* Create a vm to execute a job on the host
 *
 * @param name: the name of the vm
 * @param cores: number of cores of the vm
 * @param duration: duration of the job inside the vm
 */
simgrid::s4u::VirtualMachine* GreenHost::create_vm(std::string name, int cores, double duration)
{
    xbt_assert(state_ == GreenHost::state::ON || state_ == GreenHost::state::POWERING_ON, "create_vm: cannot create vm, wrong host state.");
    xbt_assert(this->get_spare_cores() >= cores, "create vm: cannot create vm, not enough cores available on host.");
    simgrid::s4u::VirtualMachine* vm = new simgrid::s4u::VirtualMachine(name, host_, cores, cores * 1E9);
    this->add_vm(vm);
    vm->start();
    sg_green_vm_start(vm, duration);
    return vm;
}


/* Add a vm to the set of vm on this host
 *
 * @param vm: the vm to add to the set
 */
void GreenHost::add_vm(simgrid::s4u::VirtualMachine* vm)
{
    xbt_assert(state_ == GreenHost::state::ON || state_ == GreenHost::state::POWERING_ON, "add_vm: cannot add vm, wrong host state.");
    xbt_assert(this->get_spare_cores() >= vm->get_core_count(), "add_vm: cannot create vm, not enough cores available on host.");
    vms_.emplace(vm);
}

/* Remove a vm from the list of vm on this host
 *
 * @param vm: the vm to remove from the set
 */
void GreenHost::remove_vm(simgrid::s4u::VirtualMachine* vm)
{
    vms_.erase(vm);
    on_vm_removed(*host_);
}

/* Return the unordered set of vms on this host
 */
std::unordered_set<simgrid::s4u::VirtualMachine*> GreenHost::get_vms()
{
    return vms_;
}

/* Actor used to migrate a vm from a host to another
 *
 * @param vm: the vm to migrate
 * @param host: the host receiving the vm
 *
 * Used in migrate_vm()
 * DO NOT USE DIRECTLY
 */
static void migrator_actor(simgrid::s4u::VirtualMachine* vm, simgrid::s4u::Host* host)
{
    simgrid::s4u::ActorPtr dummy_task = simgrid::s4u::Actor::create("receiver_" + vm->get_name(), host, infinite_task);
    dummy_task->daemonize();
    sg_vm_migrate(vm, host);
    simgrid::s4u::this_actor::get_host()->extension<GreenHost>()->remove_vm(vm);
    dummy_task->kill();
    XBT_INFO("MIGRATING DONE");
}

/* Migrate a vm from this host to another host
 *
 * @param vm: the vm to migrate
 * @param host: the host receiving the vm
 */
void GreenHost::migrate_vm(simgrid::s4u::VirtualMachine* vm, simgrid::s4u::Host* host)
{
    xbt_assert(vms_.count(vm) > 0, "migrate_vm: cannot migrate vm from this host, vm is not there.");
    xbt_assert(!sg_vm_is_migrating(vm), "migrate_vm: cannot migrate a vm already migrating.");
    xbt_assert(host != host_, "migrate_vm: source and destination host cannot be the same.");
    GreenHost::state recv_state = host->extension<GreenHost>()->get_state();
    xbt_assert(recv_state == GreenHost::state::ON || recv_state == GreenHost::state::POWERING_ON, "Receiver in wrong state");

    XBT_VERB("GreenHost::migrate_vm: Migrating %s from %s to %s", vm->get_name().c_str(), host_->get_name().c_str(), host->get_name().c_str());

    host->extension<GreenHost>()->add_vm(vm);
    simgrid::s4u::ActorPtr sender = simgrid::s4u::Actor::create("sender_" + vm->get_name(), host_, migrator_actor, vm, host);
    sender->daemonize();
}

double GreenHost::get_boot_time()
{
    return boot_time_;
}

/* Actor used to keep host in init state for boot duration
 *
 * Used in turn_on()
 * DO NOT USE DIRECLTY
 */
static void powering_on_manager()
{
    simgrid::s4u::Host* host = simgrid::s4u::this_actor::get_host();
    simgrid::s4u::this_actor::sleep_for(host->extension<GreenHost>()->get_boot_time());
    host->set_pstate(0);
    host->extension<GreenHost>()->set_state(GreenHost::state::ON);
}

/* Put the host in the ON state if possible, has no effect if the host
 * is in ON or POWERING_ON state.
 * Hosts powering on can have vms but cannot run them
 */
void GreenHost::turn_on()
{
    simgrid::s4u::ActorPtr actor;
    switch (state_) {
    case GreenHost::state::POWERING_OFF :
        XBT_INFO("turn_on: cannot turn on a powering off host.");
        exit(0);
        break;
    case GreenHost::state::OFF :
        state_ = GreenHost::state::POWERING_ON;
        boot_date_ = simgrid::s4u::Engine::get_instance()->get_clock();
        host_->turn_on();
        host_->set_pstate(1);
        actor = simgrid::s4u::Actor::create("powering_on_manager", host_, powering_on_manager);
        actor->daemonize();
        XBT_VERB("turn_on: powering on %s", host_->get_name().c_str());
        break;
    case GreenHost::state::EMPTYING :
        state_ = GreenHost::state::ON;
        break;
    case GreenHost::state::ON :
        break;
    case GreenHost::state::POWERING_ON :
        break;
    default:
        XBT_INFO("turn_on: bad host state");
        exit(0);
        break;
    }
}

/* Actor used to kill an host
 *
 * This actor is placed on the master host
 * Used in powering_off_manager()
 * DO NOT USE DIRECTLY
 */
static void killer_actor(simgrid::s4u::Host* host)
{
    host->turn_off();
    XBT_VERB("killer_actor: %s is now off", host->get_name().c_str());
    host->set_pstate(1);
}

/* Actor used to keep host in exit state for shutdown duration
 *
 * Used in turn_off()
 * DO NOT USE DIRECTLY
 */
static void powering_off_manager()
{
    simgrid::s4u::Host* host = simgrid::s4u::this_actor::get_host();
    simgrid::s4u::this_actor::sleep_for(10);
    host->extension<GreenHost>()->set_state(GreenHost::state::OFF);
    simgrid::s4u::ActorPtr killer = simgrid::s4u::Actor::create("killer_" + host->get_name(), simgrid::s4u::Engine::get_instance()->get_all_hosts().front(), killer_actor, host);
    killer->daemonize();
}

/* Turn off the host
 * Fail if there is still vms on the host
 * While shutting down new vms are refused
 */
void GreenHost::turn_off()
{
    simgrid::s4u::ActorPtr actor;
    switch (state_) {
    case GreenHost::state::POWERING_OFF :
        break;
    case GreenHost::state::OFF :
        break;
    case GreenHost::state::EMPTYING :
        if (!(this->is_empty()))
            break;
    case GreenHost::state::ON :
        if (!(this->is_empty()))
        {
            XBT_INFO("turn_off: cannot turn off host, there are still vms here");
            break;
        }
        state_ = GreenHost::state::POWERING_OFF;
        host_->set_pstate(2);
        actor = simgrid::s4u::Actor::create("powering_off_manager", host_, powering_off_manager);
        actor->daemonize();
        XBT_VERB("turn_off: powering off %s", host_->get_name().c_str());
        break;
    case GreenHost::state::POWERING_ON :
        XBT_INFO("turn_off: cannot turn off a powering on host");
        break;
    default:
        XBT_INFO("turn_off: bad host state");
        exit(0);
        break;
    }
}

void GreenHost::set_last_update(int v) {
    last_update_ = v;
}

/* Return the number of cores not used on the host
 */
int GreenHost::get_spare_cores()
{
    int in_use = 0;
    for (auto vm: vms_)
    {
        in_use += vm->get_core_count();
    }
    return host_->get_core_count() - in_use;
}

/* Return true if there is no vm on the host
 */
bool GreenHost::is_empty()
{
    return vms_.size() == 0;
}

/* Return the current state of the host
 *
 * See green_host.hpp for the definition of the states
 */
GreenHost::state GreenHost::get_state()
{
    return state_;
}

/* Set the current state of the host
 *
 * @param st: the state
 *
 * See green_host.hpp for the definition of the states
 */
void GreenHost::set_state(GreenHost::state st)
{
    state_ = st;
}

/* Return a string version of the current state of the host
 */
std::string GreenHost::state_string()
{
    switch (state_)
    {
    case (GreenHost::state::POWERING_ON): return "POWERING_ON";
    case (GreenHost::state::ON): return "ON";
    case (GreenHost::state::EMPTYING): return "EMPTYING";
    case (GreenHost::state::POWERING_OFF): return "POWERING_OFF";
    case (GreenHost::state::OFF): return "OFF";
    default: XBT_INFO("state_string: wrong state");
    }
}

/* Return the current CPU utilization ratio of the host
 */
double GreenHost::utilization()
{
    return 1.0 - (double)this->get_spare_cores() / (double)host_->get_core_count();
}

/* Return true if the host is underutilized
 */
bool GreenHost::is_underutilized()
{
    return this->utilization() < underutilized_threshold_;
}

/* Set the datacenter of this host
 */
void GreenHost::set_dc(DataCenter* dc)
{
    dc_ = dc;
}

/* Get a pointer to the datacenter of this host
 */
DataCenter* GreenHost::get_dc()
{
    if (dc_ != 0)
        return dc_;
    else
    {
        XBT_INFO("get_dc: datacenter not defined.");
        exit(0);
    }
}

void GreenHost::set_green_power(double p)
{
    on_green_power_change(*host_);
    green_power_ = p;
}

double GreenHost::get_green_power()
{
    return green_power_;
}

double GreenHost::get_spare_green_power()
{
    return green_power_ - sg_host_get_current_consumption(host_);
}

void GreenHost::update_consumed_energy()
{
    int now = simgrid::s4u::Engine::get_instance()->get_clock();
    if (now > last_update_)
    {
        double energy_consumed = sg_host_get_current_consumption(host_) * (now - last_update_);
        double green_energy_produced = green_power_ * (now - last_update_);
        brown_energy_consumed_ += std::max(energy_consumed - green_energy_produced, 0.0);
        green_energy_consumed_ += std::min(energy_consumed, green_energy_produced);
        last_update_ = now;
    }
}

double GreenHost::get_green_energy_consumed()
{
    return green_energy_consumed_;
}

double GreenHost::get_brown_energy_consumed()
{
    return brown_energy_consumed_;
}

void GreenHost::add_neighboor(simgrid::s4u::Host* host, double latency)
{
    neighbors_.insert(std::pair<double, simgrid::s4u::Host*>(latency, host));
}

std::multimap<double, simgrid::s4u::Host*> GreenHost::get_neighbors()
{
    return neighbors_;
}

void GreenHost::set_checked(bool b)
{
    checked_ = b;
}

bool GreenHost::is_checked()
{
    return checked_;
}

void GreenHost::uncheck_hosts(std::vector<simgrid::s4u::Host*> hosts)
{
    for (auto const& host: hosts)
        host->extension<GreenHost>()->set_checked(false);
}

void GreenHost::add_latency_initial(double l)
{
    latencies_initial_.push_back(l);
}

std::vector<double> GreenHost::get_latencies_initial()
{
    return latencies_initial_;
}

void GreenHost::add_latency_final(double l)
{
    latencies_final_.push_back(l);
}

std::vector<double> GreenHost::get_latencies_final()
{
    return latencies_final_;
}

void GreenHost::job_reception(int cores, double duration, int policy, double max_latency)
{
    xbt_assert(host_->extension<GreenHost>()->valid_host_, "job_reception: host not valid to receive a job");
    XBT_VERB("job_reception: job received on %s", host_->get_name().c_str());
    static int id = 0;
    std::pair<simgrid::s4u::Host*, double> host;
    switch (policy)
    {
    case 0:
        host = find_node_latency(host_, cores, max_latency);
        break;
    case 1:
        host = find_node_green_first(host_, cores, max_latency);
        break;
    case 2:
        host = find_node_green_most(host_, cores, max_latency);
        break;
    default:
        XBT_INFO("job_reception: bad scheduling policy provided (%d)", policy);
    }

    if (host.first != 0)
    {
        xbt_assert(host.first->extension<GreenHost>()->valid_host_, "job,reception: host not valid to run a job");
        XBT_VERB("job_reception: job placed on %s", host.first->get_name().c_str());
        host.first->extension<GreenHost>()->turn_on();
        simgrid::s4u::VirtualMachine* vm = host.first->extension<GreenHost>()->create_vm("job_" + std::to_string(id++), cores, duration);
        vm->extension<GreenVm>()->set_reception_host(host_);
        vm->extension<GreenVm>()->set_latency_bound(max_latency);

        if (simgrid::s4u::Engine::get_instance()->get_clock() >= start_log) {
            host.first->extension<GreenHost>()->add_latency_final(host.second);
            if (host.first->extension<GreenHost>()->get_state() == GreenHost::state::POWERING_ON)
                host.first->extension<GreenHost>()->add_latency_initial(boot_time_ + host.second);
            else
                host.first->extension<GreenHost>()->add_latency_initial(host.second);
        }
    }
    else
    {
        XBT_INFO("job_reception: no suitable host found");
    }
}

// on cherche le noeud qui a la place d'héberger le job et avec le moins de latence
std::pair<simgrid::s4u::Host*, double> GreenHost::find_node_latency(simgrid::s4u::Host* host, int cores, double max_latency)
{
    // hosts already visited are "checked" to avoid cycle in the graph of nodes, the nodes visited are "unchecked" at the end, when a good host is found
    std::vector<simgrid::s4u::Host*> checked_hosts;
    // hosts which may contain the job
    std::multimap<double, simgrid::s4u::Host*> potential_hosts;
    // first possible host
    potential_hosts.insert(std::pair<double, simgrid::s4u::Host*>(0.0, host));
    // pointer to the current host being verified
    std::multimap<double, simgrid::s4u::Host*>::iterator it = potential_hosts.begin();
    // host to add to the potential hosts
    std::multimap<double, simgrid::s4u::Host*> next_hosts;

    simgrid::s4u::Host* host_found = 0;
    double cumulated_latency;
    double latency;

    // we don't count the reception on the first node
    messages_sent_--;

    //on continue si on peux (encore des noeud à test) ET on a rien trouvé
    while (it != potential_hosts.end() && host_found == 0)
    {
        messages_sent_++;
        it->second->extension<GreenHost>()->set_checked();
        checked_hosts.push_back(it->second);

        // il a la place, est valide, et pas powering off: c'est le bon car la map est toujours triée par latence
        if (it->second->extension<GreenHost>()->get_spare_cores() >= cores
                && it->second->extension<GreenHost>()->get_state() != GreenHost::state::POWERING_OFF
                && it->second->extension<GreenHost>()->is_valid_host())
        {
            host_found = it->second;
            latency = it->first;
        }
        // il a pas la place, mais peux être que ses voisins ont la place et ont une latence plus faible que ceux actuels
        else
        {
            next_hosts = it->second->extension<GreenHost>()->get_neighbors();
            cumulated_latency = it->first;
            potential_hosts.erase(it);
            for (auto const& h: next_hosts)
            {
                if (!(h.second->extension<GreenHost>()->is_checked()))
                    potential_hosts.insert(std::pair<double, simgrid::s4u::Host*>(cumulated_latency + h.first, h.second));
            }
            it = potential_hosts.begin();
        }
        while (it != potential_hosts.end() && it->second->extension<GreenHost>()->is_checked())
        {
            potential_hosts.erase(it);
            it = potential_hosts.begin();
        }
    }
    // fin: on "uncheck" les host qu'on a parcouru puis en renvoi l'host, trouvé ou non
    uncheck_hosts(checked_hosts);
    if (!host_found)
        XBT_INFO("find_node_latency: no host found");
    else
    {
        return std::pair<simgrid::s4u::Host*, double> (host_found, latency);
    }
}

// on chercher le premier noeud qui a de l'énergie verte non utilisée, on s'arrete si on dépasse une certaine latence
std::pair<simgrid::s4u::Host*, double> GreenHost::find_node_green_first(simgrid::s4u::Host* host, int cores, double max_latency)
{
    // hosts already visited are "checked" to avoid cycle in the graph of nodes, the nodes visited are "unchecked" at the end, when a good host is found
    std::vector<simgrid::s4u::Host*> checked_hosts;
    // hosts which may contain the job
    std::multimap<double, simgrid::s4u::Host*> potential_hosts;
    // first possible host
    potential_hosts.insert(std::pair<double, simgrid::s4u::Host*>(0.0, host));
    // pointer to the current host being verified
    std::multimap<double, simgrid::s4u::Host*>::iterator it = potential_hosts.begin();
    // host to add to the potential hosts
    std::multimap<double, simgrid::s4u::Host*> next_hosts;

    simgrid::s4u::Host* green_host_found = 0;
    simgrid::s4u::Host* host_found = 0;
    double cumulated_latency;
    double latency;

    messages_sent_--;

    while (it != potential_hosts.end() && !green_host_found && (it->first <= max_latency || (!host_found && !green_host_found)))
    {
        messages_sent_++;
        it->second->extension<GreenHost>()->set_checked();
        checked_hosts.push_back(it->second);

        // il a la place, il est valide et pas powering_off
        if (it->second->extension<GreenHost>()->get_spare_cores() >= cores
                && it->second->extension<GreenHost>()->is_valid_host()
                && it->second->extension<GreenHost>()->get_state() != GreenHost::state::POWERING_OFF)
        {
            // il a de l'énergie verte en stock -> on prend
            if (it->second->extension<GreenHost>()->get_spare_green_power() > 0)
            {
                green_host_found = it->second;
                latency = it->first;
            }
            // on a rien trouvé encore -> on prend comme solution de secours
            else if (!host_found)
            {
                host_found = it->second;
                latency = it->first;
            }
        }
        next_hosts = it->second->extension<GreenHost>()->get_neighbors();
        cumulated_latency = it->first;
        potential_hosts.erase(it);
        for (auto const& h: next_hosts)
        {
            if (!(h.second->extension<GreenHost>()->is_checked()))
                potential_hosts.insert(std::pair<double, simgrid::s4u::Host*>(cumulated_latency + h.first, h.second));
        }
        it = potential_hosts.begin();
        while (it != potential_hosts.end() && it->second->extension<GreenHost>()->is_checked())
        {
            potential_hosts.erase(it);
            it = potential_hosts.begin();
        }
    }
    // fin: on "uncheck" les host qu'on a parcouru puis en renvoi l'host, trouvé ou non
    uncheck_hosts(checked_hosts);
    if (!green_host_found && !host_found)
        XBT_INFO("find_node_green_first: no host found");
    else if (green_host_found)
        return std::pair<simgrid::s4u::Host*, double> (green_host_found, latency);
    else
        return std::pair<simgrid::s4u::Host*, double> (host_found, latency);
}

// on cherche le noeud qui a le plus d'énergie verte non utilisée, on s'arrete si on dépasse une certaine latence
std::pair<simgrid::s4u::Host*, double> GreenHost::find_node_green_most(simgrid::s4u::Host* host, int cores, double max_latency)
{
    // hosts already visited are "checked" to avoid cycle in the graph of nodes, the nodes visited are "unchecked" at the end, when a good host is found
    std::vector<simgrid::s4u::Host*> checked_hosts;
    // hosts which may contain the job
    std::multimap<double, simgrid::s4u::Host*> potential_hosts;
    // first possible host
    potential_hosts.insert(std::pair<double, simgrid::s4u::Host*>(0.0, host));
    // pointer to the current host being verified
    std::multimap<double, simgrid::s4u::Host*>::iterator it = potential_hosts.begin();
    // host to add to the potential hosts
    std::multimap<double, simgrid::s4u::Host*> next_hosts;

    simgrid::s4u::Host* green_host_found = 0;
    double best_green_power = 0;
    simgrid::s4u::Host* host_found = 0;
    double cumulated_latency;
    double latency;

    messages_sent_--;

    while (it != potential_hosts.end() && (it->first <= max_latency || (!host_found && !green_host_found)))
    {
        messages_sent_++;
        it->second->extension<GreenHost>()->set_checked();
        checked_hosts.push_back(it->second);

        // il a la place, il est valide et n'est pas powering_off
        if (it->second->extension<GreenHost>()->get_spare_cores() >= cores
                && it->second->extension<GreenHost>()->is_valid_host()
                && it->second->extension<GreenHost>()->get_state() != GreenHost::state::POWERING_OFF)
        {
            // il a de l'énergie verte en stock -> on prend
            if (it->second->extension<GreenHost>()->get_spare_green_power() > best_green_power)
            {
                green_host_found = it->second;
                latency = it->first;
            }
            // on a rien trouvé encore -> on prend comme solution de secours
            else if (!host_found)
            {
                host_found = it->second;
                latency = it->first;
            }
        }
        // il a pas la place, mais peux être que ses voisins ont la place et ont une latence plus faible que ceux actuels
        next_hosts = it->second->extension<GreenHost>()->get_neighbors();
        cumulated_latency = it->first;
        potential_hosts.erase(it);
        for (auto const& h: next_hosts)
        {
            if (!(h.second->extension<GreenHost>()->is_checked()))
                potential_hosts.insert(std::pair<double, simgrid::s4u::Host*>(cumulated_latency + h.first, h.second));
        }
        it = potential_hosts.begin();
        while (it != potential_hosts.end() && it->second->extension<GreenHost>()->is_checked())
        {
            potential_hosts.erase(it);
            it = potential_hosts.begin();
        }
    }
    // fin: on "uncheck" les host qu'on a parcouru puis en renvoi l'host, trouvé ou non
    uncheck_hosts(checked_hosts);
    if (!green_host_found && !host_found)
        XBT_INFO("find_node_green_first: no host found");
    else if (green_host_found)
        return std::pair<simgrid::s4u::Host*, double> (green_host_found, latency);
    else
        return std::pair<simgrid::s4u::Host*, double> (host_found, latency);
}

// called when a job end
void GreenHost::consolidate()
{
    if(this->is_empty())
    {
        this->turn_off();
    }
}

// called when a job end, turn off if empty and no spare green energy
void GreenHost::consolidate_brown()
{
    if(this->is_empty() && this->get_green_power() == 0)
    {
        this->turn_off();
    }
}

//count nodes in a specific latency range, sort by state: (ON, OFF, OTHER)
std::vector <simgrid::s4u::Host*> GreenHost::get_neighborhood(double range)
{ 
    // hosts already visited are "checked" to avoid cycle in the graph of nodes, the nodes visited are "unchecked" at the end
    std::vector<simgrid::s4u::Host*> checked_hosts;
    // hosts which may contain the job
    std::multimap<double, simgrid::s4u::Host*> potential_hosts;
    // first possible host
    potential_hosts.insert(std::pair<double, simgrid::s4u::Host*>(0.0, host_));
    // pointer to the current host being verified
    std::multimap<double, simgrid::s4u::Host*>::iterator it = potential_hosts.begin();
    // hosts to add to the potential hosts
    std::multimap<double, simgrid::s4u::Host*> next_hosts;
    double cumulated_latency;

    while (it != potential_hosts.end() && it->first <= range)
    {
        it->second->extension<GreenHost>()->set_checked();
        checked_hosts.push_back(it->second);
        next_hosts = it->second->extension<GreenHost>()->get_neighbors();
        cumulated_latency = it->first;
        potential_hosts.erase(it);
        for (auto const& h: next_hosts)
            if (!(h.second->extension<GreenHost>()->is_checked()))
                potential_hosts.insert(std::pair<double, simgrid::s4u::Host*>(cumulated_latency + h.first, h.second));
        it = potential_hosts.begin();
    }
    uncheck_hosts(checked_hosts);
    return checked_hosts;
}

// called when a job end, turn off if empty and enough other nodes near
// ratio = ratio of nodes to keep alive
// threshold = if utilization of the node is above this threshold, it is overutilized
void GreenHost::consolidate_ratio(double ratio, double threshold)
{
    std::vector <simgrid::s4u::Host*> neighborhood_unclear = this->get_neighborhood(0.002);
    std::vector <simgrid::s4u::Host*> neighborhood;
    // create a list containing only valid neighbors
    for (auto const& h: neighborhood_unclear) {
        if (h->extension<GreenHost>()->is_valid_host()) {
            neighborhood.push_back(h);
        }
    }


    int on_hosts = 0;
    for (auto const& h: neighborhood)
        if ((h->extension<GreenHost>()->get_state() == GreenHost::state::ON || h->extension<GreenHost>()->get_state() == GreenHost::state::POWERING_ON)
                && h->extension<GreenHost>()->utilization() <= threshold) {
            on_hosts++;
        }
    if(this->is_empty() && this->get_green_power() == 0 && (double)(on_hosts - 1) / (double)neighborhood.size() >= ratio) {
        this->turn_off();
    }
    else
    {
        int nb_to_wake_up = ceil((double)neighborhood.size() * ratio - on_hosts);
        int i = 0;
        while (i < neighborhood.size() && nb_to_wake_up > 0)
        {
            if (neighborhood[i]->extension<GreenHost>()->get_state() == GreenHost::state::OFF)
            {
                neighborhood[i]->extension<GreenHost>()->turn_on();
                nb_to_wake_up--;
            }
            i++;
        }
    }
}

// called when green energy run out
void GreenHost::ask_for_green()
{
    if (this->get_spare_green_power() < 0)
    {
        for (auto const& vm: vms_)
        {
            std::pair<simgrid::s4u::Host*, double> host = find_node_green_first(vm->extension<GreenVm>()->get_reception_host(), vm->get_core_count(), vm->extension<GreenVm>()->get_latency_bound());
            if (host.first != 0)
            {
                XBT_VERB("ask_for_green: job moved from %s to %s", host_->get_name().c_str(), host.first->get_name().c_str());
                host.first->extension<GreenHost>()->turn_on();
                //~ this->migrate_vm(vm, host.first);
                vm->extension<GreenVm>()->set_latency_current(host.second);
            }
        }
    }
}

double GreenHost::global_power_consumption_ = 0;
double GreenHost::global_brown_power_consumption_ = 0;
double GreenHost::global_green_power_production_ = 0;
double GreenHost::global_exploitable_green_power_ = 0;
std::vector<simgrid::s4u::Host*> GreenHost::modified_consumption_hosts_ = {};
double GreenHost::time_previous_modified_consumption_ = 0;

void GreenHost::update_logs(std::vector<simgrid::s4u::Host*> hosts, std::string folder_name, int start, int end, bool force_end)
{
    static int end_ = false;
    simgrid::s4u::Engine* e = simgrid::s4u::Engine::get_instance();
    double current_time = e->get_clock();
    if ((current_time >= end || force_end) && not end_) {
        end_ = true;
        time_previous_modified_consumption_ = current_time < end ? current_time : end;
        for (auto const& h: hosts)
            h->extension<GreenHost>()->update_consumed_energy();
        write_consumed_energy_hosts(hosts, folder_name);
        write_latency_summary(hosts, folder_name);
        write_logs(folder_name);
        for (auto act: e->get_all_hosts().front()->get_all_actors())
            act->kill();
    }
    if ((current_time != time_previous_modified_consumption_ || current_time == 0) && not end_) {
        for (auto const host: modified_consumption_hosts_) {
            global_power_consumption_ -= host->extension<GreenHost>()->previous_power_consumption_;
            global_power_consumption_ += sg_host_get_current_consumption(host);
            global_brown_power_consumption_ -= host->extension<GreenHost>()->previous_brown_power_consumption_;
            global_brown_power_consumption_ += std::max(sg_host_get_current_consumption(host) - host->extension<GreenHost>()->green_power_, 0.0);
            global_green_power_production_ -= host->extension<GreenHost>()->previous_green_power_production_;
            global_green_power_production_ += host->extension<GreenHost>()->green_power_;
            global_exploitable_green_power_ -= std::min(host->extension<GreenHost>()->previous_green_power_production_, sg_host_get_wattmax_at(host, host->get_pstate()));
            global_exploitable_green_power_ += std::min(host->extension<GreenHost>()->green_power_, sg_host_get_wattmax_at(host, host->get_pstate()));
        }
        modified_consumption_hosts_.clear();
        if (current_time > start)
        {
            write_logs(folder_name);
        }
    }
    if (last_log_time_ != current_time)
    {
        modified_consumption_hosts_.push_back(host_);
        time_previous_modified_consumption_ = current_time;
        previous_power_consumption_ = sg_host_get_current_consumption(host_);
        previous_brown_power_consumption_ = std::max(sg_host_get_current_consumption(host_) - green_power_, 0.0);
        previous_green_power_production_ = green_power_;
    }
    last_log_time_ = current_time;
}


void GreenHost::write_logs(std::string folder_name)
{
    std::ofstream file;
    file.open(folder_name + "/platform_power.txt", std::ios::app);
    file << std::to_string(time_previous_modified_consumption_) + "," +
            std::to_string(global_power_consumption_) + "," +
            std::to_string(global_brown_power_consumption_) + "," +
            std::to_string(global_green_power_production_) + "," +
            std::to_string(global_exploitable_green_power_) + "\n";
    file.close();
}

void GreenHost::set_valid_host(bool b)
{
    valid_host_ = b;
}

bool GreenHost::is_valid_host()
{
    return valid_host_;
}

int GreenHost::get_message_sent()
{
    return messages_sent_;
}

void GreenHost::init_global_values(int start_log) {
    double green_power;
    double power;
    auto hosts = simgrid::s4u::Engine::get_instance()->get_all_hosts();
    for (auto const& host: hosts) {
        green_power = host->extension<GreenHost>()->get_green_power();
        power = sg_host_get_current_consumption(host);
        GreenHost::global_power_consumption_ += power;
        GreenHost::global_brown_power_consumption_ += std::max(power - green_power, 0.0);
        GreenHost::global_green_power_production_ += std::min(green_power, power);
        GreenHost::global_exploitable_green_power_ += std::min(green_power, sg_host_get_wattmax_at(host, 0));
    }
    simgrid::s4u::Actor::create("init_links_log", hosts.front(), actor_links_initial_consumption, start_log);
}

std::vector<simgrid::s4u::Host*> GreenHost::get_valid_hosts() {
    std::vector<simgrid::s4u::Host*> valid_hosts;
    for (auto const h: simgrid::s4u::Engine::get_instance()->get_all_hosts()) {
        if (h->extension<GreenHost>()->is_valid_host())
            valid_hosts.push_back(h);
    }
    return valid_hosts;
}

/* Compute a map where keys are the different district of the hosts
 * and the items are the hosts in this district
 *
 * @return: the map
 */
std::map<int, std::vector<simgrid::s4u::Host*>> GreenHost::get_hosts_per_district() {
    std::map<int, std::vector<simgrid::s4u::Host*>> hosts_per_district;
    std::string name = "";
    std::string s = "";
    for (auto const& h: GreenHost::get_valid_hosts()) {
        s = "";
        name = h->get_name();
        for (auto const& c:name) {
            if (c == '_')
                break;
            if (c != 'U')
                s += c;
        }
        hosts_per_district[stoi(s)].push_back(h);
    }
    return hosts_per_district;
}

void GreenHost::create_green_production_actors(int green_producers, std::map<int, std::vector<simgrid::s4u::Host*>> hosts_per_district) {
    XBT_VERB("create_green_production_actors: start");
    double nb_districts_same_trace = 260.0 / 35.0; // we split the 35 pv production traces over the 260 districts
    std::vector<simgrid::s4u::Host*> current_hosts;
//    std::vector<simgrid::s4u::Host*> hosts_for_current_trace;
    std::vector<std::vector<simgrid::s4u::Host*>> hosts_per_trace;
    int trace_number;
    int random_host_number;

    for (auto const& district: hosts_per_district) {
        trace_number = int(district.first / nb_districts_same_trace + 1); //[1, 35]
        if (trace_number > hosts_per_trace.size()) {
             hosts_per_trace.push_back(std::vector<simgrid::s4u::Host*>());
        }
        current_hosts = district.second;
        for (unsigned i = 0; i < green_producers && current_hosts.size() > 0; i++) {
            random_host_number = rand() % current_hosts.size();
            hosts_per_trace[trace_number - 1].push_back(current_hosts[random_host_number]);
            current_hosts.erase(current_hosts.begin() + random_host_number);
        }
    }
    XBT_VERB("create_green_production_actors: creating pv_production actors");
    for (int i = 1; i < 36; i++) {
        simgrid::s4u::ActorPtr act = simgrid::s4u::Actor::create(
                    "pv_production_trace_" + std::to_string(i),
                    simgrid::s4u::Engine::get_instance()->get_all_hosts().front(),
                    pv_production_actor_host,
                    "../photo-voltaic/normalized_traces/normalized_trace-" + std::to_string(i) + ".csv", hosts_per_trace[i-1]);
        act->daemonize();
    }
    XBT_VERB("create_green_production_actors: end");
}

void pv_production_actor_host(std::string file, std::vector<simgrid::s4u::Host*> hosts)
{
    std::ifstream f(file);
    xbt_assert(f.is_open(),"Could not open file %s", file.c_str());
    std::string time;
    std::string production;
    while(std::getline(f, time, ';'))
    {
        std::getline(f, production, '\n');
        simgrid::s4u::this_actor::sleep_until(atoi(time.c_str()));
        for (auto const& host: hosts)
            host->extension<GreenHost>()->set_green_power(atof(production.c_str()));
    }
}

/* Deduce the energy consumed by the link before the log has started
 *
 */
void actor_links_initial_consumption(int init_log) {
    simgrid::s4u::this_actor::sleep_until(init_log);
    simgrid::s4u::Engine::get_instance()->get_all_hosts().front()->extension<GreenHost>()->links_energy_consumed_before_log_ = get_links_energy_consumed();
}




