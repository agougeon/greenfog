#ifndef GREEN_HOST
#define GREEN_HOST

#include "simgrid/s4u.hpp"

/* green vm extend the class VirtualMachine in simgrid by adding new
 * attributes and function.
 *
 * The extension is added to an existing vm when it is started
 * (and not when it is created).
 */

/* Initialize the plugin
 */
void sg_green_vm_init();

/* Start the job inside the vm (vm will be killed at the end of the duration)
 *
 * @param vm: the vm in which we want to its job
 * @param duration: duration of the job inside the vm
 */
void sg_green_vm_start(simgrid::s4u::VirtualMachine* vm, double duration);

/* Return the remaining duration of the job inside the vm
 *
 * @param vm: the vm
 */
double sg_green_vm_get_remaining_duration(simgrid::s4u::VirtualMachine* vm);

class GreenVm{
public:
    static simgrid::xbt::Extension<simgrid::s4u::Host, GreenVm> EXTENSION_ID;

    explicit GreenVm(simgrid::s4u::VirtualMachine* vm) : vm_(vm), start_time_(-1), duration_(0) {}
    ~GreenVm() = default;

    /* Return the time when the vm was started
   */
    double get_start_time();

    /* Set the start time of the vm
   *
   * @param time: the time to set
   */
    void set_start_time(double time);

    /* Return the remaining duration of the job inside the vm
   */
    double get_remaining_duration();

    /* Begin the computing of the job in the vm
 *
 * @param duration: duration of the job inside the vm
 */
    void start_computing(double duration);

    void set_reception_host(simgrid::s4u::Host* h);

    simgrid::s4u::Host* get_reception_host();

    void set_latency_bound(double l);

    double get_latency_bound();

    void set_latency_initial(double l);

    double get_latency_initial();

    void set_latency_current(double l);

    double get_latency_current();

private:
    /* The vm extended by the plugin
   */
    simgrid::s4u::VirtualMachine* vm_;

    /* The start time of the vm
   */
    double start_time_;

    /* The duration of the job inside the vm
   */
    double duration_;

    simgrid::s4u::Host* reception_host_;

    double latency_bound_;

    double latency_initial_;

    double latency_current_;



};


#endif
