#ifndef TOOLS
#define TOOLS

#include "simgrid/s4u.hpp"
#include "datacenter.hpp"

std::string exepath();
void sort(int array[][3], int fst_dim);
void print_status(std::vector<simgrid::s4u::Host*> hosts);
std::vector<DataCenter*> split_hosts_by_datacenter(std::vector<simgrid::s4u::Host*> hosts);
void print_consumed_energy_dc(std::vector<DataCenter*> dcs);
void print_consumed_energy_hosts(std::vector<simgrid::s4u::Host*> hosts);
void write_consumed_energy_hosts(std::vector<simgrid::s4u::Host*> hosts, std::string folder_name);
void print_latency_summary(std::vector<simgrid::s4u::Host*> hosts);
void write_latency_summary(std::vector<simgrid::s4u::Host*> hosts, std::string folder_name);
void compute_routes(std::vector<simgrid::s4u::Host*> hosts);
void create_logs(std::string prefix, std::string platform, std::string config, std::string jobs);
void plugins_init();
void argument_checker(int argc, char* argv[]);
std::vector<double> config_parameters(std::string config_file);
void init_hosts(int init_state, int start_log);
void signals_connection(std::string log_folder, std::vector<simgrid::s4u::Host*> hosts, int start_log, int end_log);
void apply_consolidation_policy(int consolidation_policy, double ratio, double threshold);
int count_jobs(std::string jobs_file);
std::vector<int> get_weight_vector();
double get_links_energy_consumed(void);
#endif
