#ifndef TASKS
#define TASKS

/* Task with very high flops, MUST be killed
 */
void infinite_task();

/* Task to do for an actor
 *
 * @param w: flops to do
 */
void task(int w);

/* Task executing only 1 flop
 */
void unit_task();

#endif
