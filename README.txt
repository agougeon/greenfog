-----------------
| PREREQUISITES |
-----------------

Simgrid is mandatory to run the simulation.

Before the installation of SimGrid replace the files energy.h and 
host_energy.cpp with those located in the folder "simgrid_files".


Example command:
./greenfog PLATFORM_FILE(.xml) CONFIG_FILE JOB_FILE LOG_FOLDER
