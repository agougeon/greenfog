-------------
| JOB FILES |
-------------

Job files are composed of a line of integers for each job such as:
ARRIVAL_DATE CORES_NEEDED DURATION

Valid values are as follows:

ARRIVAL_DATE:
any positive integer

CORES_NEEDED:
any integer above 0 (between 1 and 4 included, in our platform)

DURATION:
any integer above 0

Example with 4 jobs:
0 4 10
1 3 10
2 2 10
3 1 10



--- jobs are assumed to be sorted by arrival date! ---
