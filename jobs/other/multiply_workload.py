#!/usr/bin/python3

# Multiply jobs of file "aggregated_jobs.txt" into another file
# Used to scale workload to the platform used

import random
import math
import numpy as np
import matplotlib.pyplot as plt

def rebuild(infile, outfile, scaling=1):
  f=open(infile, "r")
  g=open(outfile, "w")
  for line in f:
    words=line.split(";")
    if int(words[2]) > 4:
      g.write(words[0] + " " + str(4) + " " + words[3])
    else:
      g.write(words[0] + " " + words[2] + " " + words[3])
  f.close()
  g.close()
  
def multiply_jobs(outfile, factor):
  f=open("aggregated_jobs.txt", "r")
  g=open(outfile, "w")
  for line in f:
    if line[-1] != '\n':
      line = line + '\n'
    for i in range(0, factor):
      g.write(line)
  f.close()
  g.close()

def get_peak_cores(filename):
  f=open(filename, "r")
  active_jobs = []
  peak = 0
  for line in f:
    words = line.split()
    active_jobs.append((float(words[0]) + float(words[2]), int(words[1])))
    current_cores_used = 0
    i = 0
    while(True):
      if i >= len(active_jobs):
        break;
      else:
        if active_jobs[i][0] < float(words[0]):
          active_jobs.pop(i)
        else:
          current_cores_used += active_jobs[i][1]
      i+=1
    if current_cores_used > peak:
          peak = current_cores_used
  return peak

def get_next_stop(job_list):
	mini = -1
	for cores,end in job_list:
		if mini == -1 or end < mini:
			mini = end
	return mini

def show_core_used(job_file):
	f=open(job_file, "r")
	cores_total = 1964 * 4
	cores_used = 0
	next_stop = -1
	active_jobs = []
	x = [0]
	y = [0]
	nb_jobs = 0
	for line in f:
		tokens = line.split(" ")
		job_cores = int(tokens[1])
		job_start = int(tokens[0])
		job_end = int(tokens[2]) + job_start
		if job_start > 86400:
			nb_jobs += 1
		if job_start > 259200:
			break
		while (next_stop <= job_start and next_stop != -1):
			index_to_pop = []
			for i in range(len(active_jobs)):
				cores = active_jobs[i][0]
				end = active_jobs[i][1]
				if end == next_stop:
					index_to_pop.append(i)
					cores_used -= cores
			for i in range(len(index_to_pop)):
				active_jobs.pop(index_to_pop[i]-i)
			x.append(next_stop)
			y.append(cores_used)
			next_stop = get_next_stop(active_jobs)
			# ~ exit(0)
		active_jobs.append((job_cores, job_end))
		cores_used += job_cores
		next_stop = job_end if job_end < next_stop or next_stop == -1 else next_stop
		if x[-1] == job_start:
			y[-1] = cores_used
		else:
			x.append(job_start)
			y.append(cores_used)
	y = [i / cores_total for i in y]
	print(nb_jobs)
	plt.plot(x,y)
	plt.show()

show_core_used("../eucalyptus_workload_scaled/eucalyptus_workload_scaled_88.txt")

# ~ multiply_jobs("eucalyptus_workload_scaled.txt", 6)
# ~ print(get_peak_cores("../eucalyptus_workload_scaled/eucalyptus_workload_scaled_88.txt"))
