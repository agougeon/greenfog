#!/usr/bin/python3

# Aggregate the trace files and output a file coherent with our program
# Tasks demanding more than 4 cores are reduced to 4 cores.

import random
import math
import numpy as np
import matplotlib.pyplot as plt
import os

# date init / nb cores / duration
def aggregate_traces(folder):
  files = [f for f in os.listdir(folder)]
  d = dict()
  jobs = []
  for f in files:
    trace = open(f, "r")
    for l in trace:
      words = l.rstrip().split(";")
      if words[2] in d:
        (init, cores) = d[words[2]]
        jobs.append([int(init), cores, str(int(words[0]) - int(init))])
        d.pop(words[2])
      else:
        d[words[2]] = (words[0], words[3])

  output = open("aggregated_jobs.txt", "w")
  jobs.sort()
  for j in jobs:
    output.write(str(j[0]) + " " + j[1] + " " + j[2] + "\n")

def limit_cores(infile, outfile, limit):
  f=open(infile, "r")
  g=open(outfile, "w")
  for line in f:
    words=line.split()
    if int(words[1]) > 4:
      g.write(words[0] + " " + str(4) + " " + words[2] + "\n")
    else:
      g.write(words[0] + " " + words[1] + " " + words[2] + "\n")
  f.close()
  g.close()
  os.system("mv aggregated_jobs_cores_limit.txt aggregated_jobs.txt")

  
aggregate_traces("../eucalyptus_workload/")
limit_cores("aggregated_jobs.txt", "aggregated_jobs_cores_limit.txt", 4)
