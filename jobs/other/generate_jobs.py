#!/usr/bin/python3

# Generate random jobs 

import random
import math
import numpy as np
import matplotlib.pyplot as plt

def generate_jobs(height, width, start, total_duration):
  f=open("generated_jobs.txt", "w")
  f.close()
  t = start
  times = []
  jobs = []
  a = height
  c = width
  while t < total_duration - 1500:
    b = int(t/86400) * 86400 + 43200
    times.append(t)
    try:
      nb_jobs = int(a * math.exp(-1*(t - b)**2/(2*c**2)))
    except OverflowError:
      nb_jobs = 0
    jobs.append(nb_jobs)
    t += 0.1
  
  less_jobs = [jobs[i] for i in range(0, len(jobs), 10000)]
  less_times = [int(times[i]) for i in range(0, len(jobs), 10000)]
  print(less_times)
  plt.plot(less_times, less_jobs)
  plt.show()
  
  f=open("generated_jobs.txt", "w")
  for (t,n) in zip(less_times, less_jobs):
    for i in range(0,n):
      f.write(str(t) + " " + str(random.randint(1,4)) + " " + str(random.randint(750,1500)) + "\n")

generate_jobs(1000, 20000, 1000, 86400)
