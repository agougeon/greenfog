#!/usr/bin/python3

import matplotlib.pyplot as plt
import numpy as np
import os
import time
import datetime
import progressbar as pb # pip3 install progressbar2

class Duration:
	def __init__(self, time_file=""):
		f = open(time_file, "r")
		tokens = f.readlines()[1].split(",")
		f.close()
		self.start = int(tokens[0])
		self.end = int(tokens[1])
		self.duration = int(tokens[2])

class Config:
	def __init__(self, config_file=""):
		f = open(config_file, "r")
		lines = f.readlines()
		f.close()
		tokens=lines[0].split(" ")
		self.init = int(tokens[0])
		self.sched = int(tokens[1])
		self.consol = int(tokens[2])
		self.green_prod = int(tokens[3])
		self.lat_range = float(tokens[4])
		self.ratio = float(tokens[5])
		self.thresh = float(tokens[6])
		self.platform = lines[1].split(" ")[1]
		self.jobs = lines[2].split(" ")[1]
		if "58" in self.jobs:
			self.jobs = "58%"
		elif "73" in self.jobs:
			self.jobs = "73%"
		elif "88" in self.jobs:
			self.jobs = "88%"

	def __repr__(self):
		return "(" + str(self.init) + ", " \
				   + str(self.sched) +  ", " \
				   + str(self.consol) +  ", " \
				   + str(self.green_prod) +  ", " \
				   + str(self.lat_range) +  ", " \
				   + str(self.ratio) +  ", " \
				   + str(self.thresh) + ", " \
				   + self.jobs + ")"

class Latency:
	def __init__(self, latency_file="", latency_list=[]):
		if latency_list != [] and latency_file == "":
			self.init_list(latency_list)
			return
		f = open(latency_file, "r")
		lines = f.readlines()
		f.close()
		headers = lines[0].split(",")
		tokens = lines[1].split(",")
		self.jobs_per_lat = [int(tokens[i]) for i in range(0,20)]
		self.jobs_per_sector = [int(tokens[i]) for i in range(20,25)]
		self.jobs_awaking = int(tokens[25])
		self.jobs_not_awaking = int(tokens[26])
		self.msg = int(tokens[27])
		if (sum(self.jobs_per_lat) != sum(self.jobs_per_sector) or
			sum(self.jobs_per_lat) != (self.jobs_awaking + self.jobs_not_awaking) or
			sum(self.jobs_per_sector) != (self.jobs_awaking + self.jobs_not_awaking)):
			print("PROBLEME NOMBRE DE JOBS !!!!")

	def init_list(self, latency_list):
		self.jobs_per_lat = [lat.jobs_per_lat for lat in latency_list]
		self.jobs_per_sector = [lat.jobs_per_sector for lat in latency_list]
		self.jobs_awaking = np.mean([lat.jobs_awaking for lat in latency_list])
		self.jobs_awaking_std = np.std([lat.jobs_awaking for lat in latency_list])
		self.jobs_not_awaking = np.mean([lat.jobs_not_awaking for lat in latency_list])
		self.jobs_not_awaking_std = np.std([lat.jobs_not_awaking for lat in latency_list])
		self.msg_mean = np.mean([lat.msg for lat in latency_list])
		self.msg_std = np.std([lat.msg for lat in latency_list])

	def __repr__(self):
		return "Latency obj"

class Consumption:
	def __init__(self, consumption_file="", consumption_list=[]):
		if consumption_list != [] and consumption_file == "":
			self.init_list(consumption_list)
			return
		f = open(consumption_file, "r")
		lines = f.readlines()
		f.close()
		hosts = lines[1].split(",")
		links = lines[2].split(",")
		total = lines[3].split(",")
		self.hosts_green = float(hosts[1])
		self.hosts_brown = float(hosts[2])
		self.links_green = float(links[1])
		self.links_brown = float(links[2])
		self.total_green = float(total[1])
		self.total_brown = float(total[2])


	def init_list(self, consumption_list):
		self.hosts_green = np.mean([conso.hosts_green for conso in consumption_list])
		self.hosts_green_std = np.std([conso.hosts_green for conso in consumption_list])
		self.hosts_brown = np.mean([conso.hosts_brown for conso in consumption_list])
		self.hosts_brown_std = np.std([conso.hosts_brown for conso in consumption_list])
		self.links_green = np.mean([conso.links_green for conso in consumption_list])
		self.links_green_std = np.std([conso.links_green for conso in consumption_list])
		self.links_brown = np.mean([conso.links_brown for conso in consumption_list])
		self.links_brown_std = np.std([conso.links_brown for conso in consumption_list])
		self.total_green = np.mean([conso.total_green for conso in consumption_list])
		self.total_green_std = np.std([conso.total_green for conso in consumption_list])
		self.total_brown = np.mean([conso.total_brown for conso in consumption_list])
		self.total_brown_std = np.std([conso.total_brown for conso in consumption_list])

	def __repr__(self):
		return "Consumption obj"

class Simulation:
	def __init__(self, folder_path=""):
		if not os.path.exists(folder_path):
			return
		self.cfg = Config(folder_path + "/resume.txt")
		self.lat = Latency(folder_path + "/latency_summary.txt")
		self.conso = Consumption(folder_path + "/consumed_energy.txt")
		self.time = Duration(folder_path + "/duration.txt")
	
	def __repr__(self):
		return "Simulation obj"


# return the list of simulations as Simulation objects
def get_simulations(log_folder):
	simulations = []
	print("Reading log files...")
	for machine in pb.progressbar(os.listdir(log_folder)):
		for job in os.listdir(log_folder + "/" + machine):
			for config in os.listdir(log_folder + "/" + machine + "/" + job):
				simulations.append(Simulation(log_folder + "/" + machine + "/" + job + "/" + config))
	print(str(len(simulations)) + " simulations found")
	return simulations

def print_time_infos(sim_list):
	start = 0
	end = 0
	sum_t = 0
	for sim in sim_list:
		if sim.time.start < start or start == 0:
			start = sim.time.start
		end = max(sim.time.end, end)
		sum_t += sim.time.duration
	print("Start date: " + time.ctime(start))
	print("End date: " + time.ctime(end))
	print("Total duration: " + str(datetime.timedelta(seconds = end - start)))
	print("Average simulation duration: " + str(int(sum_t / len(sim_list))) + "s")


# return a dictionary of "config as a string: [simulations]"
# i.e.: the expe sorted by their config
def sort_simulations_by_config(sim_list):
	dic = dict()
	print("Sorting simulations by configuration...")
	for sim in sim_list:
		if str(sim.cfg) in dic:
			dic[str(sim.cfg)].append(sim)
		else:
			dic[str(sim.cfg)] = [sim]
	return dic

# return a list of simulations aggregated
# Each aggregated simulation is a mean of all simulations associated to the same config
# in the dic given as parameter
def aggregate_simulations(dic):
	l = []
	print("Aggregating simulations by configuration...")
	for cfg, sim_list in dic.items():
		e = Simulation()
		e.cfg = sim_list[0].cfg
		e.lat = Latency(latency_list=[sim.lat for sim in sim_list])
		e.conso = Consumption(consumption_list=[sim.conso for sim in sim_list])
		l.append(e)
	print("Simulations aggregated in " + str(len(l)) + " different configurations")
	return l

# print the different configs in the expe list
def show_configs(sim_list):
	configs = [sim.cfg for sim in sim_list]
	param = [
	("initial state 		", set()),
	("scheduling policy 	", set()),
	("consolidation policy    ", set()),
	("green producers		", set()),
	("latency range		", set()),
	("ratio 			", set()),
	("threshold 		",set()),
	("jobs 			",set())
	]
	for c in configs:
		param[0][1].add(c.init)
		param[1][1].add(c.sched)
		param[2][1].add(c.consol)
		param[3][1].add(c.green_prod)
		param[4][1].add(c.lat_range)
		param[5][1].add(c.ratio)
		param[6][1].add(c.thresh)
		param[7][1].add(c.jobs)
	print("Config founds in the list of simulations:")
	for key,values in param:
		print(key + ": " + str(sorted(list(values)))[1:-1])

# plot several simulations: choose config to compare and value to show
# fixed_parameters: array of arrays of config parameter fixed, array of size 7
# example: ([0], [1], [], [], [0.01,0,10], [], [], [])
# (init, sched, consol, green_prod, lat_range, ratio, thresh, jobs)
# sim_list: the list of aggregated simulations
# value_to_plot: the value to plot on the graph: one of "energy_total, energy_brown,
# energy_green, jobs_per_lat, jobs_awaking, jobs_not_awaking, messages"
def plot_experiments(fixed_parameters, sim_list, value_to_plot):
	correct_configs = []
	for sim in sim_list:
		if ((not fixed_parameters[0] or sim.cfg.init in fixed_parameters[0]) and
		    (not fixed_parameters[1] or  sim.cfg.sched in fixed_parameters[1]) and
		    (not fixed_parameters[2] or sim.cfg.consol in fixed_parameters[2]) and
		    (not fixed_parameters[3] or sim.cfg.green_prod in fixed_parameters[3]) and
		    (not fixed_parameters[4] or sim.cfg.lat_range in fixed_parameters[4]) and
		    (not fixed_parameters[5] or sim.cfg.ratio in fixed_parameters[5]) and
		    (not fixed_parameters[6] or sim.cfg.thresh in fixed_parameters[6]) and
		    (not fixed_parameters[7] or sim.cfg.jobs in fixed_parameters[7])):
			 correct_configs.append(sim)
	print(str(len(correct_configs)) + " simulations meet the desired parameters")
	if len(correct_configs) == 0:
		print("plot_experiments: nothing to plot, maybe there is no experiment matching the fixed parameters")
		exit(0)
	################################################
	if value_to_plot == "energy_total":
		values = []
		values2 = []
		values_std = []
		values2_std = []
		x_labels = []
		for sim in correct_configs:
			values.append(sim.conso.hosts_brown)
			values2.append(sim.conso.hosts_green)
			
			values_std.append(sim.conso.hosts_brown_std)
			values2_std.append(sim.conso.hosts_green_std)
			
			x_labels.append(str(sim.cfg)) 

		title = "Total Energy Consumed"
		filename = "total_energy_consumed"
		y_label = "Energy Consumed (J)"
	################################################
	elif value_to_plot == "energy_brown":
		values = []
		values_std = []
		x_labels = []
		for sim in correct_configs:
			values.append(sim.conso.brown)
			values_std.append(sim.conso.brown_std)
			x_labels.append(str(sim.cfg)) 

		title = "Brown Energy Consumed"
		filename = "brown_energy_consumed"
		y_label = "Brown Energy Consumed (J)"
	################################################
	elif value_to_plot == "energy_green":
		values = []
		values_std = []
		x_labels = []
		for sim in correct_configs:
			values.append(sim.conso.green)
			values_std.append(sim.conso.green_std)
			x_labels.append(str(sim.cfg))

		title = "Green Energy Consumed"
		filename = "Green_energy_consumed"
		y_label = "Green Energy Consumed (J)"
	################################################
	elif value_to_plot == "jobs_per_lat":
		nb_jobs = sum(correct_configs[0].lat.jobs_per_sector[0])
		jobs_per_lat = [sim.lat.jobs_per_lat for sim in correct_configs]
		transposes = [np.transpose(t).tolist() for t in jobs_per_lat]
		for i in range(1,len(transposes)):
			for j in range(len(transposes[i])):
				transposes[0][j] += transposes[i][j]
		transposes = transposes[0]
		
		values = [np.mean(t) for t in transposes]
		values_std = [np.std(t) for t in transposes]
		x_labels = [i for i in range(len(correct_configs[0].lat.jobs_per_lat[0]))]

		title = "Jobs per latency"
		filename = "jobs_per_latency"
		y_label = "Jobs (unit)"
	################################################
	elif value_to_plot == "jobs_per_sector":
		jobs_per_sector = [sim.lat.jobs_per_sector for sim in correct_configs]
		transposes = [np.transpose(t).tolist() for t in jobs_per_sector]
		for i in range(1,len(transposes)):
			for j in range(len(transposes[i])):
				transposes[0][j] += transposes[i][j]
		transposes = transposes[0]
		
		values = [np.mean(t) for t in transposes]
		values_std = [np.std(t) for t in transposes]
		x_labels = [i for i in range(len(correct_configs[0].lat.jobs_per_lat[0]))]

		title = "Jobs per sector"
		filename = "jobs_per_sector"
		y_label = "Jobs (unit)"
	################################################
	elif value_to_plot == "jobs_awaking":
		values = [sim.lat.jobs_awaking for sim in correct_configs]
		values_std = [sim.lat.jobs_awaking_std for sim in correct_configs]
		x_labels = [str(sim.cfg) for sim in correct_configs]

		title = "Jobs placed on booting host"
		filename = "jobs_awaking"
		y_label = "Jobs (unit)"
	################################################
	elif value_to_plot == "jobs_not_awaking":
		values = []
		values_std = []
		x_labels = []
		for sim in correct_configs:
			values.append(sim.lat.jobs_not_awaking)
			values_std.append(sim.lat.jobs_not_awaking_std)
			x_labels.append(str(sim.cfg))

		title = "Jobs placed on hosts already running"
		filename = "jobs_not_awaking"
		y_label = "Jobs (unit)"
	################################################
	elif value_to_plot == "messages":
		values = []
		values_std = []
		x_labels = []
		for sim in correct_configs:
			values.append(sim.lat.msg_mean)
			values_std.append(sim.lat.msg_std)
			x_labels.append(str(sim.cfg))

		title = "Messages Exchanged"
		filename = "messages_exchanged"
		y_label = "Messages Exchanged (unit)"
	else:
		print("plot_experiments: wrong value to plot")


	fig, ax = plt.subplots()
	x_pos = np.arange(len(correct_configs))
	if value_to_plot == "jobs_per_lat":
		x_pos = [i for i in range(len(correct_configs[0].lat.jobs_per_lat[0]))]
	elif value_to_plot == "jobs_per_sector":
		x_pos = [i for i in range(len(correct_configs[0].lat.jobs_per_sector[0]))]
	if value_to_plot != "jobs_per_lat" and value_to_plot != "jobs_per_sector":
		values, values_std, x_labels = (list(t) for t in zip(*sorted(zip(values, values_std, x_labels))))
	if value_to_plot == "energy_total":
		ax.bar(x_pos, values, yerr=values_std, align='center', alpha=0.5, ecolor='black', capsize=10, color="brown")
		ax.bar(x_pos, values2, yerr=values2_std, bottom=values, align='center', alpha=0.5, ecolor='black', capsize=10, color="green")
	else:
		ax.bar(x_pos, values, yerr=values_std, align='center', alpha=0.5, ecolor='black', capsize=10)

	ax.set_ylabel(y_label)
	ax.set_xticks(x_pos)
	ax.set_xticklabels(x_labels)
	ax.set_title(title)
	ax.yaxis.grid(True)
	#~ plt.tight_layout()
	if value_to_plot != "jobs_per_lat" and value_to_plot != "jobs_per_sector":
		plt.xticks(rotation=90)
		plt.subplots_adjust(top=0.95, bottom=0.50)
	plt.savefig("figures/" + filename)
	plt.show()
	
def global_comparison(fixed_parameters,sim_list,f):
	correct_configs = []
	for sim in sim_list:
		if ((not fixed_parameters[0] or sim.cfg.init in fixed_parameters[0]) and
		    (not fixed_parameters[1] or  sim.cfg.sched in fixed_parameters[1]) and
		    (not fixed_parameters[2] or sim.cfg.consol in fixed_parameters[2]) and
		    (not fixed_parameters[3] or sim.cfg.green_prod in fixed_parameters[3]) and
		    (not fixed_parameters[4] or sim.cfg.lat_range in fixed_parameters[4]) and
		    (not fixed_parameters[5] or sim.cfg.ratio in fixed_parameters[5]) and
		    (not fixed_parameters[6] or sim.cfg.thresh in fixed_parameters[6]) and
		    (not fixed_parameters[7] or sim.cfg.jobs in fixed_parameters[7])):
			 correct_configs.append(sim)
	print(str(len(correct_configs)) + " simulations meet the desired parameters")
	if len(correct_configs) == 0:
		print("plot_experiments: nothing to plot, maybe there is no experiment matching the fixed parameters")
		exit(0)
	######################################################
	lines = []
	for sim in correct_configs:
		nb_jobs = sum(sim.lat.jobs_per_sector[0])
		jobs_per_sector = [np.mean(t) for t in np.transpose(sim.lat.jobs_per_sector).tolist()]
		hosts_total = sim.conso.hosts_green + sim.conso.hosts_brown
		line = []
		for v in jobs_per_sector:
			line.append(round(v / nb_jobs * 100, 1))
		line.append(round(sim.lat.jobs_awaking / nb_jobs * 100, 1))
		line.append(round(sim.lat.jobs_not_awaking / nb_jobs * 100, 1))
		line.append(round(sim.conso.hosts_green / hosts_total * 100, 1))
		line.append(sim.conso.hosts_brown)
		line.append(str(sim.cfg))
		lines.append(line)
	lines.sort(key=lambda sim: sim[8])
	fi = open(f, "w+")
	for line in lines:
		fi.write(str(line))
		fi.write("\n")
	fi.close


sim_list = get_simulations("log_22-30-06_21-04")
sim_dic = sort_simulations_by_config(sim_list)
sim_aggregated = aggregate_simulations(sim_dic)
# ~ print_time_infos(sim_list)

#~ (INIT_STATE, SCHEDULING_POLICY, CONSOLIDATION_POLICY, GREEN_PRODUCERS, LATENCY_RANGE, RATIO, THRESHOLD, jobs)
# ~ plot_experiments(([0],[0,1],[1,2],[1],[],[],[],["73%"]), sim_aggregated, "jobs_awaking")
# ~ plot_experiments(([0],[0,1],[1,2],[1],[],[],[],["73%"]), sim_aggregated, "energy_total")
global_comparison(([0],[1],[],[1],[0.02],[],[],["73%"]),sim_aggregated,"global_comparison.txt")
