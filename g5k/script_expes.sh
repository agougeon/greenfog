#!/bin/bash

##########################
# DO NOT RUN THIS SCRIPT #
#   RUN G5K-SCRIPT.SH    #
##########################

cd install/greenfog/bin

# Var for the execution of the simulations
EXEC=greenfog
PLATFORM=../platforms/italyISP.xml
JOBS=../jobs/eucalyptus_workload_scaled/$2
CONFIGS=../configs/configs
TOTAL=$(ls $CONFIGS -1 | wc -l)
i=0
mkdir -p ~/log/$1/$2

# Var used to observe the progress of the simulations
mkdir -p ~/progress/$1
PROGRESS_FILE=~/progress/$1/$2

while [ $i -lt $TOTAL ]
do
	# build log file
	LOG=~/log/$1/$2/config_$i
	mkdir -p $LOG

	# execution
	init_date=`date +%s`
	#~ ./$EXEC $PLATFORM $CONFIGS/config_$i.txt $JOBS $LOG --log=initilization_fog.threshold:verbose --log=green_host.threshold:verbose --log=scheduling.threshold:verbose &> $LOG/stdout.txt
	./$EXEC $PLATFORM $CONFIGS/config_$i.txt $JOBS $LOG &> /dev/null
	end_date=`date +%s`
	echo "start,end,duration" > $LOG/duration.txt
	echo "$init_date,$end_date,$(($end_date - $init_date))" >> $LOG/duration.txt

	# Unused file for averaging results
	rm $LOG/platform_power.txt

	# update progress file
	((i++))
	echo "$(($i * 100 / $TOTAL))" > $PROGRESS_FILE
done
