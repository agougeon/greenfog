#!/bin/bash

#----------------#
# SET YOU G5K ID #
#----------------#

G5K_ID="agougeon"
################################
write_command_result() {
case "$?" in
	0) echo "SUCCESS $1" ;;
	128) echo "SKIPPED $1: folder already present" ;;
	*) echo "FAILURE $1 : $?" ;;
esac
}

print_install_results() {
while IFS= read -r line; do
	if [ ${line::2} = 'SU' ]
	then
		echo -e "[\e[32mSUCCESS\e[39m] ${line:8}"
	elif	[ ${line::2} = 'SK' ]
	then
		echo -e "[\e[33mSKIPPED\e[39m] ${line:8}"
	else
		echo -e "[\e[31mFAILURE\e[39m] ${line:8}"
	fi
done < install/install_result.txt
}

check_folders() {
		if [ -d "install/code" ] && [ -d "install/FMI4cpp" ] && [ -d "install/pandapower-fmu" ] && [ -d "install/simgrid" ]	&& [ -d "install/simgrid-FMI" ]
		then
				:
		else
				echo "Necessary folders are missing or you are running this script from an incorrect location. Exiting."
				exit
		fi
}

run_one_node(){
	./script_expes.sh $1 eucalyptus_workload_scaled_58.txt &
	./script_expes.sh $1 eucalyptus_workload_scaled_73.txt &
	./script_expes.sh $1 eucalyptus_workload_scaled_88.txt &
}
################################

if [ -z "$1" ]
then
echo "Using g5k ID \"$G5K_ID\""
echo "USAGE:"
echo "$0 reservation	(from local) reserve nodes on g5K"
echo "$0 upload		(from local) upload the bash scripts"
echo "$0 install		(from the job on g5k) install necessary tools"
echo "$0 run		(from the job on g5k) run simulations"
echo "$0 progress	(from the job on g5k) show the progression of simulations"
echo "$0 download	(from local) download log files"
exit 0
fi
##################################
if [ $1 = "reservation" ]
then
	if [ -z $2 ] || [ -z $3 ]
	then
		echo "$0 $1 NB_NODES WALLTIME"
	else
		NB_NODES=$2
		WALLTIME=$3
		echo "Reserving node(s) on rennes site"
		ssh rennes.g5k "oarsub -p \"cluster not in ('parapluie')\" -l nodes=$NB_NODES,walltime=$WALLTIME \"sleep 10d\""
		job=$(ssh rennes.g5k "oarstat | grep $G5K_ID")
		echo "To connect to the job go to the platform and use:"
		echo "\$oarsub -C JOB_ID"
	fi
##################################
elif [ $1 = "upload" ]
then
	scp g5k-script.sh $G5K_ID@access.grid5000.fr:rennes
	scp script_expes.sh $G5K_ID@access.grid5000.fr:rennes
#################################
elif [ $1 = "install" ]
then
	mkdir -p install && cd install
	:> install_result.txt
	COMMAND_RESULT_FILE=$PWD/install_result.txt
	NODES=$(uniq $OAR_NODEFILE)
	for NODE in ${NODES:2}
	do
		oarsh $G5K_ID@$NODE "sudo-g5k apt-get update" &
		oarsh $G5K_ID@$NODE "sudo-g5k apt-get install -y cmake build-essential libboost-dev libboost-all-dev git" &
	done
	# install required packages
	sudo-g5k apt-get update
	sudo-g5k apt-get install -y cmake build-essential libboost-dev libboost-all-dev git unzip
	write_command_result "installing packages" >> $COMMAND_RESULT_FILE
	# pull simgrid
	git clone https://framagit.org/simgrid/simgrid.git
	write_command_result "git clone SimGrid" >> $COMMAND_RESULT_FILE
	cd simgrid
	git pull
	write_command_result "git pull SimGrid" >> $COMMAND_RESULT_FILE
	git checkout f225e52fef082a934584fce772560fc730bfca48
	write_command_result "git checkout SimGrid" >> $COMMAND_RESULT_FILE # after this checkout the program does not work anymore 
	cd
	# pull greenfog 
	cd install
	git clone https://gitlab.inria.fr/agougeon/greenfog.git
	write_command_result "git clone greenfog" >> $COMMAND_RESULT_FILE
	cd greenfog
	git pull
	write_command_result "git pull greenfog" >> $COMMAND_RESULT_FILE
	cd
	# updating simgrid files
	cp install/greenfog/simgrid_files/energy.h install/simgrid/include/simgrid/plugins
	write_command_result "copy energy.h to simgrid" >> $COMMAND_RESULT_FILE
	cp install/greenfog/simgrid_files/host_energy.cpp install/simgrid/src/plugins
	write_command_result "copy host_energy.h to simgrid" >> $COMMAND_RESULT_FILE
	# install simgrid
	cd install/simgrid
	cmake -Denable_documentation=OFF -Denable_java=OFF -Denable_smpi=OFF -DCMAKE_INSTALL_PREFIX=/home/$G5K_ID/install/simgrid .
	write_command_result "cmake SimGrid" >> $COMMAND_RESULT_FILE
	make install -j4
	write_command_result "make install SimGrid" >> $COMMAND_RESULT_FILE
	cd
	# install greenfog
	cd install/greenfog
	mkdir -p build 
	cd build
	cmake -DSimGrid_PATH=/home/$G5K_ID/install/simgrid ..
	write_command_result "cmake greenfog" >> $COMMAND_RESULT_FILE
	make -j4
	write_command_result "make greenfog" >> $COMMAND_RESULT_FILE
	cd ../configs
	python3 generate_configs.py -f configs
	write_command_result "generate configs" >> $COMMAND_RESULT_FILE
	cd
	# echo install result
	echo
	echo "##################"
	echo "# INSTALL RESULT #"
	echo "##################"
	print_install_results
################################
elif [ $1 = "run" ]
then
	NODES=$(uniq $OAR_NODEFILE)
	for NODE in $NODES
	do
		oarsh $G5K_ID@$NODE "./g5k-script.sh worker $NODE" &
	done
################################
elif [ $1 = "worker" ]
then
	NODE=$2
	run_one_node $NODE
##################################
elif [ $1 = "progress" ]
then
	NB_JOBS=3 # value depending on the number of job file running on a single machine (see above)
	for MACHINE in $(ls progress/)
	do
	SUM=0
		for JOB in $(ls progress/$MACHINE)
		do
			FILE_VAL=$(cat progress/$MACHINE/$JOB)
			SUM=$(($SUM + $FILE_VAL))
			PERCENTAGE=$(($SUM / $NB_JOBS))
		done
		echo "$PERCENTAGE% $MACHINE"
	done
#################################
elif [ $1 = "download" ]
then
	date=`date "+%H-%M-%S_%d-%m"`
	ssh rennes.g5k "cd /home/$G5K_ID/log && zip -r log_$date.zip *"
	scp -r $G5K_ID@access.grid5000.fr:rennes/log/log_${date}.zip .
	ssh rennes.g5k "cd /home/$G5K_ID/log && rm log_$date.zip"
	unzip -d log_$date log_$date.zip
#################################
elif [ $1 = "clean" ]
then
	rm -rf execution_results.txt progress OAR* public
	read -r -p "Also delete log dir? [y/N] " response
	if [[ "$response" =~ ^([yY](es)*)$ ]]
	then
		rm -rf log
	fi
##################################
else
	echo "wrong parameter, make sure you known what you are doing"
fi


