-------------
| Grid 5000 |
-------------

The goal of this folder is to use provided the scripts to run many experiment in parallel 
on the grid 5000 platform

#################################

-----------------
| g5k-script.sh |
-----------------

The script "g5k-script.sh" will allow you:
  - on YOUR MACHINE to:
    + reserve nodes on the platform
    + upload the 2 bash scripts in your home on grid 5000
    + download the results
  - in YOUR GRID 5000 HOME and once you are connect to your job to :
    + install the necessary dependancies
    + run the experiments

#################################

------------------
| scrip_expes.sh |
------------------

should not be used direclty
