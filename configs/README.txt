----------------
| CONFIG FILES |
----------------

Config files are composed of one unique line of integers as follows:
INIT_STATE SCHEDULING_POLICY CONSOLIDATION_POLICY GREEN_PRODUCERS LATENCY_RANGE RATIO THRESHOLD

Valid values are as follows:

INIT_STATE:
0: hosts initially off
1: hosts initially on

SCHEDULING_POLICY (FOG):
0: optimize latency
1: first green
2: most green

CONSOLIDATION_POLICY (FOG)
0: no consolidation
1: consolidate all
2: consolidate brown
3: consolidate ratio

GREEN_PRODUCERS (FOG)
number of green producers per district

LATENCY_RANGE (FOG)
value in s defining the max latency range

RATIO (FOG -> consolidation ratio only)
value between 0 and 1: ratio of nodes to keep available in a district

THRESHOLD (FOG -> consolidation ratio only)
value between 0 and 1: if a node usage is above this threshold it is considered full (and thus not available)

##################################

--------------------
| WEIGHT PLACEMENT |
--------------------

The weights_placements.txt files determine the probability to randomly place a job in a specific district

Given the chance C (written in the file) for a distric and the total amount of chances for all district T, 
its probability to be chosen is: P = C / T

##################################

---------------------
| CONFIG GENERATION |
---------------------

The python file generate_configs.py allows to generate many configs at once
Its usage is defined inside it

#################################

-------------------------
| DEPRECATED PARAMETERS |
-------------------------

Following parameters are used for deprecated code and may not work properly.
To use those parameters you must first change the function called in the main file "greenfog.cpp"
By default the fog function is called, but you can also use the datacenter or host function.

SCHEDULING_POLICY (DC):
0: round robin
1: first fit
2: first fit optimized
3: mbfd

CONSOLIDATION_POLICY (DC)
0: no consolidation
1: first fit
2: mbfd
