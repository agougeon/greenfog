#!/usr/bin/python3

# Generate config files
# to use it run: python3 generate_configs.py -f FOLDER_NAME

import os
import argparse

# Parsing arguments
parser = argparse.ArgumentParser()
parser.add_argument('-f', '--folder', type=str, help="name of the destination folder", required=True)
args = parser.parse_args()

# Set parameters of configs to generate
initial_state = [0,1]
scheduling_policy = [0,1,2]
consolidation_policy = [0,1,2,3]
green_producers = [1,3,5,10]
latency_range = [0, 0.005, 0.01, 0.02]
ratio = [0, 0.1, 0.2, 0.3, 0.4]
threshold = [0, 0.25, 0.5, 0.75]

def generate_configs(folder):
	os.system("mkdir -p " + folder)
	i = 0
	for a in initial_state:
		for b in scheduling_policy:
			for c in consolidation_policy:
				for d in green_producers:
					for e in latency_range:
						for f in ratio:
							for g in threshold:
								if ((b == 0 and e != 0) or 
									(b != 0 and e == 0) or
									(c != 3 and (f != 0 or g != 0)) or 
									(c == 3 and (f == 0 or g == 0))):
									pass
								else:
									cf = open(folder + "/config_" + str(i) + ".txt", "w")
									cf.write(str(a) + " " + str(b) + " " + str(c) + " " + str(d) + " " + str(e) + " " + str(f) + " " + str(g) + " ")
									cf.close()
									i=i+1
	print(str(i) + " configuration generated in folder \"" + folder + "\"")

def show_configs(folder):
	files = [name for name in os.listdir(folder + "/") if (not os.path.isdir(name)) and name[0:6] == "config"]
	for f in files:
		g = open(folder + "/" + f, "r")
		lines = g.readlines()
		print(lines[0])

def rm_configs():
	os.system("rm config*")

generate_configs(args.folder)
